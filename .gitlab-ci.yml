workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

.setup_toolversion: &setup_toolversion |
  TOOLVERSION=$(cat .tool-versions | md5sum | cut -d' ' -f1)$(cat Dockerfile.ci | md5sum | cut -d' ' -f1) 
  echo "Using tool version: ${TOOLVERSION}"

stages:
  - prepare
  - test
  - version-tag
  - build
  - release
  - sast

prepare:
  stage: prepare
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  before_script:
    - *setup_toolversion
    - echo "TOOLVERSION=${TOOLVERSION}" >> build.env
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  script:
    - |
      if ! docker pull $CI_REGISTRY_IMAGE/builder:${TOOLVERSION}; then
        echo "Building new image for ${TOOLVERSION}"
        docker build -f Dockerfile.ci -t $CI_REGISTRY_IMAGE/builder:${TOOLVERSION} .
        docker push $CI_REGISTRY_IMAGE/builder:${TOOLVERSION}
      fi
  artifacts:
    reports:
      dotenv: build.env

test:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH
  before_script:
    - . /root/.asdf/asdf.sh
  image: $CI_REGISTRY_IMAGE/builder:$TOOLVERSION
  script:
    - mix deps.get
    - mix assets.setup
    - mix assets.build
    - mix test
  cache:
    key:
      files:
        - .tool-versions
        - mix.lock
    paths:
      - deps
      - _build

version-tag:
  stage: build
  image: alpine:latest
  before_script:
    - apk add --no-cache git
  rules:
    - if: $CI_COMMIT_BRANCH
  script:
    - |
      # Extract the version from mix.exs
      VERSION=$(grep 'version:' mix.exs | cut -d '"' -f2)
      
      # Fetch all tags to ensure up-to-date information
      git fetch --tags
      
      # Check if the tag already exists
      if git rev-parse "v$VERSION" >/dev/null 2>&1; then
        echo "Tag v$VERSION already exists. Skipping tag creation."
      else
        echo "Creating and pushing tag v$VERSION."
        
        # Configure Git user
        git config --global user.email "gitlab-ci@example.com"
        git config --global user.name "GitLab CI"
        
        # Create the new tag
        git tag "v$VERSION"
        
        # Add the new remote with the correct credentials
        git remote add https-origin https://gitlab-ci-token:${PERSONAL_ACCESS_TOKEN}@gitlab.com/gazzer82/up-tally.git
        
        # Push the new tag using the correct credentials
        git push https-origin "v$VERSION"
        
        echo "Tag v$VERSION pushed successfully."
      fi

build:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - . /root/.asdf/asdf.sh
  image: $CI_REGISTRY_IMAGE/builder:$TOOLVERSION
  script:
    - mix deps.get
    - mix assets.setup
    - mix assets.build
    - mix assets.deploy
    - MIX_ENV=prod mix release
    - chmod +x burrito_out/up_tally_macos
    - chmod +x burrito_out/up_tally_linux
    - cd ..
    - git clone https://gitlab-ci-token:${PERSONAL_ACCESS_TOKEN}@gitlab.com/gazzer82/up_tally_firmware.git
    - asdf install
    - cd up_tally_firmware
    - export MIX_TARGET=rpi4
    - mix deps.get
    - mix firmware
    - mix firmware.image
    - cp /builds/gazzer82/up_tally_firmware/up_tally_firmware.img /builds/gazzer82/up-tally/up_tally_firmware.img
    - cp /builds/gazzer82/up_tally_firmware/_build/rpi4_dev/nerves/images/up_tally_firmware.fw /builds/gazzer82/up-tally/up_tally_firmware.fw
  after_script:
    - echo "ARTIFACT_CI_JOB_ID=${CI_JOB_ID}" >> build.env
  artifacts:
    paths:
      - burrito_out/up_tally_windows.exe
      - burrito_out/up_tally_macos
      - burrito_out/up_tally_linux
      - up_tally_firmware.img
      - up_tally_firmware.fw
    expire_in: never
    reports:
      dotenv: build.env

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "running release_job"
  release:
    tag_name: $CI_COMMIT_TAG
    description: "Release $CI_COMMIT_TAG"
    assets:
      links:
        - name: "Windows Binary"
          url: "${CI_PROJECT_URL}/-/jobs/${ARTIFACT_CI_JOB_ID}/artifacts/raw/burrito_out/up_tally-windows.exe"
        - name: "MacOS Binary" 
          url: "${CI_PROJECT_URL}/-/jobs/${ARTIFACT_CI_JOB_ID}/artifacts/raw/burrito_out/up_tally_macos"
        - name: "Linux Binary"
          url: "${CI_PROJECT_URL}/-/jobs/${ARTIFACT_CI_JOB_ID}/artifacts/raw/burrito_out/up_tally_linux"
        - name: "Pi4 Firmware"
          url: "${CI_PROJECT_URL}/-/jobs/${ARTIFACT_CI_JOB_ID}/artifacts/raw/up_tally_firmware.fw"
        - name: "Pi4 Firmware Image"
          url: "${CI_PROJECT_URL}/-/jobs/${ARTIFACT_CI_JOB_ID}/artifacts/raw/up_tally_firmware.img"

sast:
  stage: sast
include:
  - template: Security/SAST.gitlab-ci.yml