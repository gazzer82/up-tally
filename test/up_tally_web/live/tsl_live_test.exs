defmodule UpTallyWeb.TslLiveTest do
  use UpTallyWeb.ConnCase
  use UpTally.Case

  import Phoenix.LiveViewTest
  import UpTally.ServicesFixtures

  @create_attrs %{
    interface: "127.0.0.1",
    ip: "239.1.2.3",
    name: "some name",
    port: 40001,
    tsl_version: :tsl_v3,
    send_interval: 50,
    polling_interval: 200,
    polling_enabled: true,
    tsl_offset: 1
  }
  @update_attrs %{
    interface: "127.0.0.1",
    ip: "239.1.2.4",
    name: "some updated name",
    port: 40002,
    tsl_version: :tsl_v5,
    send_interval: 55,
    polling_interval: 205,
    polling_enabled: false,
    tsl_offset: 0
  }
  @invalid_attrs %{
    interface: "127.0.0.1",
    ip: nil,
    name: nil,
    port: nil,
    tsl_version: :tsl_v3,
    send_interval: nil,
    polling_interval: nil,
    polling_enabled: false,
    tsl_offset: nil
  }

  defp create_tsl(_) do
    tsl = tsl_fixture()
    %{tsl: tsl}
  end

  describe "Index" do
    setup [:create_tsl]

    test "lists all tsl", %{conn: conn, tsl: tsl} do
      {:ok, _index_live, html} = live(conn, ~p"/tsl")

      assert html =~ "Listing Tsl"
      assert html =~ tsl.interface
    end

    test "saves new tsl", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/tsl")

      assert index_live |> element("a", "New Tsl") |> render_click() =~
               "New Tsl"

      assert_patch(index_live, ~p"/tsl/new")

      assert index_live
             |> form("#tsl-form", tsl: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#tsl-form", tsl: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/tsl")

      html = render(index_live)
      assert html =~ "Tsl created successfully"
      assert html =~ "127.0.0.1"
    end

    test "updates tsl in listing", %{conn: conn, tsl: tsl} do
      {:ok, index_live, _html} = live(conn, ~p"/tsl")

      assert index_live |> element("#tsl_collection-#{tsl.id} a", "Edit") |> render_click() =~
               "Edit Tsl"

      assert_patch(index_live, ~p"/tsl/#{tsl}/edit")

      assert index_live
             |> form("#tsl-form", tsl: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      # open_browser(index_live)

      assert index_live
             |> form("#tsl-form", tsl: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/tsl")

      html = render(index_live)
      assert html =~ "Tsl updated successfully"
      assert html =~ "127.0.0.1"
    end

    test "deletes tsl in listing", %{conn: conn, tsl: tsl} do
      {:ok, index_live, _html} = live(conn, ~p"/tsl")

      assert index_live |> element("#tsl_collection-#{tsl.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#tsl_collection-#{tsl.id}")
    end
  end
end
