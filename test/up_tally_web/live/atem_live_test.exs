defmodule UpTallyWeb.AtemLiveTest do
  use UpTallyWeb.ConnCase

  import Phoenix.LiveViewTest
  import UpTally.ServicesFixtures

  @create_attrs %{ip: "10.11.12.1", port: 42, test: false, model: "constellation_2me"}
  @update_attrs %{ip: "10.11.12.2", port: 43, test: true, model: "constellation_4me"}
  @invalid_attrs %{ip: nil, port: nil, test: false, model: "constellation_2me"}

  defp create_atem(_) do
    atem = atem_fixture()
    %{atem: atem}
  end

  describe "Index" do
    setup [:create_atem]

    test "lists all atem", %{conn: conn, atem: atem} do
      {:ok, _index_live, html} = live(conn, ~p"/atem")

      assert html =~ "Listing Atem"
      assert html =~ atem.ip
    end

    test "saves new atem", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/atem")

      assert index_live |> element("a", "New Atem") |> render_click() =~
               "New Atem"

      assert_patch(index_live, ~p"/atem/new")

      assert index_live
             |> form("#atem-form", atem: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#atem-form", atem: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/atem")

      html = render(index_live)
      assert html =~ "Atem created successfully"
      assert html =~ "10.11.12.1"
      assert html =~ "Constellation 2ME"
    end

    test "updates atem in listing", %{conn: conn, atem: atem} do
      {:ok, index_live, _html} = live(conn, ~p"/atem")

      assert index_live |> element("#atem_collection-#{atem.id} a", "Edit") |> render_click() =~
               "Edit Atem"

      assert_patch(index_live, ~p"/atem/#{atem}/edit")

      assert index_live
             |> form("#atem-form", atem: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#atem-form", atem: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/atem")

      html = render(index_live)
      assert html =~ "Atem updated successfully"
      assert html =~ "10.11.12.2"
      assert html =~ "Constellation 4ME"
    end

    test "deletes atem in listing", %{conn: conn, atem: atem} do
      {:ok, index_live, _html} = live(conn, ~p"/atem")

      assert index_live |> element("#atem_collection-#{atem.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#atem_collection-#{atem.id}")
    end
  end
end
