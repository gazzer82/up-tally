defmodule UpTallyWeb.VideohubLiveTest do
  use UpTallyWeb.ConnCase

  import Phoenix.LiveViewTest
  import UpTally.ServicesFixtures

  @create_attrs %{port: 42, ip: "some ip", name: "some name"}
  @update_attrs %{port: 43, ip: "some updated ip", name: "some updated name"}
  @invalid_attrs %{port: nil, ip: nil}

  defp create_videohub(_) do
    videohub = videohub_fixture()
    %{videohub: videohub}
  end

  describe "Index" do
    setup [:create_videohub]

    test "lists all videohub", %{conn: conn, videohub: videohub} do
      {:ok, _index_live, html} = live(conn, ~p"/videohub")

      assert html =~ "Listing Videohub"
      assert html =~ videohub.ip
    end

    test "saves new videohub", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/videohub")

      assert index_live |> element("a", "New Videohub") |> render_click() =~
               "New Videohub"

      assert_patch(index_live, ~p"/videohub/new")

      assert index_live
             |> form("#videohub-form", videohub: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#videohub-form", videohub: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/videohub")

      html = render(index_live)
      assert html =~ "Videohub created successfully"
      assert html =~ "some ip"
      assert html =~ "some name"
    end

    test "updates videohub in listing", %{conn: conn, videohub: videohub} do
      {:ok, index_live, _html} = live(conn, ~p"/videohub")

      assert index_live
             |> element("#videohub_collection-#{videohub.id} a", "Edit")
             |> render_click() =~
               "Edit Videohub"

      assert_patch(index_live, ~p"/videohub/#{videohub}/edit")

      assert index_live
             |> form("#videohub-form", videohub: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#videohub-form", videohub: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/videohub")

      html = render(index_live)
      assert html =~ "Videohub updated successfully"
      assert html =~ "some updated ip"
      assert html =~ "some updated name"
    end

    test "deletes videohub in listing", %{conn: conn, videohub: videohub} do
      {:ok, index_live, _html} = live(conn, ~p"/videohub")

      assert index_live
             |> element("#videohub_collection-#{videohub.id} a", "Delete")
             |> render_click()

      refute has_element?(index_live, "#videohub-#{videohub.id}")
    end
  end

  describe "Show" do
    setup [:create_videohub]

    test "displays videohub", %{conn: conn, videohub: videohub} do
      {:ok, _show_live, html} = live(conn, ~p"/videohub/#{videohub}")

      assert html =~ "Show Videohub"
      assert html =~ videohub.ip
    end

    test "updates videohub within modal", %{conn: conn, videohub: videohub} do
      {:ok, show_live, _html} = live(conn, ~p"/videohub/#{videohub}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Videohub"

      assert_patch(show_live, ~p"/videohub/#{videohub}/show/edit")

      assert show_live
             |> form("#videohub-form", videohub: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#videohub-form", videohub: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/videohub/#{videohub}")

      html = render(show_live)
      assert html =~ "Videohub updated successfully"
      assert html =~ "some updated ip"
    end
  end
end
