ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(UpTally.Repo, :manual)

# Setup tsl controller default mock
Mox.defmock(UpTally.Services.TslControllerMock, for: UpTally.Services.TslControllerBehaviour)
Application.put_env(:up_tally, :tsl_controller, UpTally.Services.TslControllerMock)

# Setup tsl server default mock
Mox.defmock(UpTally.Services.TslServerMock, for: UpTally.Services.TslServerBehaviour)
Application.put_env(:up_tally, :tsl_server, UpTally.Services.TslServerMock)
