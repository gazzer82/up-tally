defmodule UpTally.ServicesTest do
  use UpTally.DataCase
  use UpTally.Case

  alias UpTally.Services

  describe "tsl" do
    alias UpTally.Services.Tsl

    import UpTally.ServicesFixtures

    @invalid_attrs %{
      name: nil,
      ip: "1923.168.5.4",
      interface: "127.0052.0.1",
      port: nil,
      tsl_version: :tsl_v6,
      send_interval: 1,
      polling_interval: 1,
      polling_enabled: nil,
      tsl_offset: "hello",
      me1: true,
      me2: true,
      me3: false,
      me4: false
    }

    test "list_tsl/0 returns all tsl" do
      tsl = tsl_fixture()
      assert Services.list_tsl() == [tsl]
    end

    test "get_tsl!/1 returns the tsl with given id" do
      tsl = tsl_fixture()
      assert Services.get_tsl!(tsl.id) == tsl
    end

    test "create_tsl/1 with valid data creates a tsl" do
      valid_attrs = %{
        name: "some name",
        ip: "239.1.2.3",
        interface: "127.0.0.1",
        port: 40001,
        tsl_version: :tsl_v3,
        send_interval: 50,
        polling_interval: 200,
        polling_enabled: true,
        tsl_offset: 1,
        me1: true,
        me2: true,
        me3: false,
        me4: false
      }

      assert {:ok, %Tsl{} = tsl} = Services.create_tsl(valid_attrs)
      assert tsl.interface == "127.0.0.1"
      assert tsl.ip == "239.1.2.3"
      assert tsl.name == "some name"
      assert tsl.port == 40001
      assert tsl.tsl_version == :tsl_v3
      assert tsl.send_interval == 50
      assert tsl.polling_interval == 200
      assert tsl.polling_enabled == true
      assert tsl.tsl_offset == 1
      assert tsl.me1 == true
      assert tsl.me2 == true
      assert tsl.me3 == false
      assert tsl.me4 == false
    end

    test "create_tsl/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Services.create_tsl(@invalid_attrs)
    end

    test "update_tsl/2 with valid data updates the tsl" do
      tsl = tsl_fixture()

      update_attrs = %{
        name: "some updated name",
        ip: "239.1.2.4",
        port: 40002,
        tsl_version: :tsl_v5,
        send_interval: 55,
        polling_interval: 205,
        polling_enabled: false,
        tsl_offset: 0,
        me1: false,
        me2: false,
        me3: true,
        me4: true
      }

      assert {:ok, %Tsl{} = tsl} = Services.update_tsl(tsl, update_attrs)
      assert tsl.ip == "239.1.2.4"
      assert tsl.name == "some updated name"
      assert tsl.port == 40002
      assert tsl.tsl_version == :tsl_v5
      assert tsl.send_interval == 55
      assert tsl.polling_interval == 205
      assert tsl.polling_enabled == false
      assert tsl.tsl_offset == 0
      assert tsl.me1 == false
      assert tsl.me2 == false
      assert tsl.me3 == true
      assert tsl.me4 == true
    end

    test "update_tsl/2 with invalid data returns error changeset" do
      tsl = tsl_fixture()
      assert {:error, %Ecto.Changeset{}} = Services.update_tsl(tsl, @invalid_attrs)
      assert tsl == Services.get_tsl!(tsl.id)
    end

    test "delete_tsl/1 deletes the tsl" do
      tsl = tsl_fixture()
      assert {:ok, %Tsl{}} = Services.delete_tsl(tsl)
      assert_raise Ecto.NoResultsError, fn -> Services.get_tsl!(tsl.id) end
    end

    test "change_tsl/1 returns a tsl changeset" do
      tsl = tsl_fixture()
      assert %Ecto.Changeset{} = Services.change_tsl(tsl)
    end
  end

  describe "atem" do
    alias UpTally.Services.Atem

    import UpTally.ServicesFixtures

    @invalid_attrs %{ip: nil, port: nil, test: false, model: ""}

    test "list_atem/0 returns all atem" do
      atem = atem_fixture()
      assert Services.list_atem() == [atem]
    end

    test "get_atem!/1 returns the atem with given id" do
      atem = atem_fixture()
      assert Services.get_atem!(atem.id) == atem
    end

    test "create_atem/1 with valid data creates a atem" do
      valid_attrs = %{ip: "some ip", port: 42, test: false, model: "constellation_2me"}

      assert {:ok, %Atem{} = atem} = Services.create_atem(valid_attrs)
      assert atem.ip == "some ip"
      assert atem.port == 42
      assert atem.test == false
      assert atem.model == "constellation_2me"
    end

    test "create_atem/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Services.create_atem(@invalid_attrs)
    end

    test "update_atem/2 with valid data updates the atem" do
      atem = atem_fixture()
      update_attrs = %{ip: "some updated ip", port: 43, test: true, model: "constellation_4me"}

      assert {:ok, %Atem{} = atem} = Services.update_atem(atem, update_attrs)
      assert atem.ip == "some updated ip"
      assert atem.port == 43
      assert atem.test == true
      assert atem.model == "constellation_4me"
    end

    test "update_atem/2 with invalid data returns error changeset" do
      atem = atem_fixture()
      assert {:error, %Ecto.Changeset{}} = Services.update_atem(atem, @invalid_attrs)
      assert atem == Services.get_atem!(atem.id)
    end

    test "delete_atem/1 deletes the atem" do
      atem = atem_fixture()
      assert {:ok, %Atem{}} = Services.delete_atem(atem)
      assert_raise Ecto.NoResultsError, fn -> Services.get_atem!(atem.id) end
    end

    test "change_atem/1 returns a atem changeset" do
      atem = atem_fixture()
      assert %Ecto.Changeset{} = Services.change_atem(atem)
    end
  end

  describe "videohub" do
    alias UpTally.Services.Videohub

    import UpTally.ServicesFixtures

    @invalid_attrs %{port: nil, ip: nil}

    test "list_videohub/0 returns all videohub" do
      videohub = videohub_fixture()
      assert Services.list_videohub() == [videohub]
    end

    test "get_videohub!/1 returns the videohub with given id" do
      videohub = videohub_fixture()
      assert Services.get_videohub!(videohub.id) == videohub
    end

    test "create_videohub/1 with valid data creates a videohub" do
      valid_attrs = %{port: 42, ip: "some ip", name: "some name"}

      assert {:ok, %Videohub{} = videohub} = Services.create_videohub(valid_attrs)
      assert videohub.port == 42
      assert videohub.ip == "some ip"
      assert videohub.name == "some name"
    end

    test "create_videohub/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Services.create_videohub(@invalid_attrs)
    end

    test "update_videohub/2 with valid data updates the videohub" do
      videohub = videohub_fixture()
      update_attrs = %{port: 43, ip: "some updated ip", name: "some updated name"}

      assert {:ok, %Videohub{} = videohub} = Services.update_videohub(videohub, update_attrs)
      assert videohub.port == 43
      assert videohub.ip == "some updated ip"
      assert videohub.name == "some updated name"
    end

    test "update_videohub/2 with invalid data returns error changeset" do
      videohub = videohub_fixture()
      assert {:error, %Ecto.Changeset{}} = Services.update_videohub(videohub, @invalid_attrs)
      assert videohub == Services.get_videohub!(videohub.id)
    end

    test "delete_videohub/1 deletes the videohub" do
      videohub = videohub_fixture()
      assert {:ok, %Videohub{}} = Services.delete_videohub(videohub)
      assert_raise Ecto.NoResultsError, fn -> Services.get_videohub!(videohub.id) end
    end

    test "change_videohub/1 returns a videohub changeset" do
      videohub = videohub_fixture()
      assert %Ecto.Changeset{} = Services.change_videohub(videohub)
    end
  end
end
