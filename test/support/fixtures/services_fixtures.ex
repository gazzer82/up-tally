defmodule UpTally.ServicesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `UpTally.Services` context.
  """

  @doc """
  Generate a tsl.
  """
  def tsl_fixture(attrs \\ %{}) do
    {:ok, tsl} =
      attrs
      |> Enum.into(%{
        name: "some name",
        ip: "239.1.2.3",
        interface: "127.0.0.1",
        port: 40001,
        tsl_version: :tsl_v3,
        send_interval: 50,
        polling_interval: 200,
        polling_enabled: true,
        tsl_offset: 1,
        me1: true,
        me2: true,
        me3: false,
        me4: false
      })
      |> UpTally.Services.create_tsl()

    tsl
  end

  @doc """
  Generate a atem.
  """
  def atem_fixture(attrs \\ %{}) do
    {:ok, atem} =
      attrs
      |> Enum.into(%{
        ip: "10.11.12.1",
        port: 42,
        test: false,
        model: "constellation_2me"
      })
      |> UpTally.Services.create_atem()

    atem
  end

  @doc """
  Generate a videohub.
  """
  def videohub_fixture(attrs \\ %{}) do
    {:ok, videohub} =
      attrs
      |> Enum.into(%{
        port: 42,
        ip: "some ip",
        name: "some name"
      })
      |> UpTally.Services.create_videohub()

    videohub
  end
end
