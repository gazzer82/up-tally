defmodule UpTally.Case do
  use ExUnit.CaseTemplate

  setup _ do
    Mox.stub_with(UpTally.Services.TslControllerMock, UpTally.Services.TslControllerStub)
    Mox.stub_with(UpTally.Services.TslServerMock, UpTally.Services.TslServerStub)
    :ok
  end
end
