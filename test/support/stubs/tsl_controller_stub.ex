defmodule UpTally.Services.TslControllerStub do
  @behaviour UpTally.Services.TslControllerBehaviour
  def config_updated() do
  end

  def get_status() do
    %{unknown: 0, connecting: 0, connected: 0, disconnected: 0}
  end
end
