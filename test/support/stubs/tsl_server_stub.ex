defmodule UpTally.Services.TslServerStub do
  @behaviour UpTally.Services.TslServerBehaviour

  def get_state(_id) do
    {:ok, %{status: :connected}}
  end
end
