@echo off

@echo Copying files

Xcopy /E /Y /i up_tally C:\ProgramData\up_tally

@echo Installing Service
c:\ProgramData\up_tally\erts-13.2\bin\erlsrv.exe add "up_tally_up_tally" -sname "up_tally"  -env RELEASE_ROOT="C:\ProgramData\up_tally" -env RELEASE_NAME="up_tally" -env RELEASE_VSN="0.1.1" -env RELEASE_MODE="embedded" -env RELEASE_COOKIE="PMFLCJI3IMOWLIUEM7RI5ZS37MO7BT3KOF2TYEUFTNMSIIFYGIZA====" -env RELEASE_NODE="up_tally" -env RELEASE_VM_ARGS="C:\ProgramData\up_tally\releases\0.1.1\vm.args" -env RELEASE_TMP="C:\ProgramData\up_tally\tmp" -env RELEASE_SYS_CONFIG="C:\ProgramData\up_tally\releases\0.1.1\sys"   -args "-setcookie PMFLCJI3IMOWLIUEM7RI5ZS37MO7BT3KOF2TYEUFTNMSIIFYGIZA==== -config C:\ProgramData\up_tally\releases\0.1.1\sys -mode embedded -boot C:\ProgramData\up_tally\releases\0.1.1\start -boot_var RELEASE_LIB C:\ProgramData\up_tally\lib -args_file C:\ProgramData\up_tally\releases\0.1.1\vm.args" -i "up_tally_up_tally"

@echo Setting service to auto delayed
sc config up_tally_up_tally start= delayed-auto