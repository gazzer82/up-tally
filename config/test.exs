import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :up_tally, UpTally.Repo,
  database: Path.expand("../up_tally_test.db", Path.dirname(__ENV__.file)),
  pool_size: System.schedulers_online() * 2,
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :up_tally, UpTallyWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "L/OL2qUi8UgaxyeSE/bDRq4UPdPKo5FOeNzztSNcr5TIPL+hJgoMOG5WMPLZ4HHf",
  server: false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

# Enable helpful, but potentially expensive runtime checks
config :phoenix_live_view,
  enable_expensive_runtime_checks: true

# Enable test variable
config :up_tally, :env, :test
