defmodule UpTally.MixProject do
  use Mix.Project

  @app :up_tally

  def project do
    [
      app: @app,
      version: "2.4.2",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      # releases: [{@app, releases()}]
      releases: releases()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {UpTally.Application, []},
      extra_applications: [:logger, :runtime_tools, :crypto, :logger_file_backend]
    ]
  end

  # defp releases do
  #   [
  #     up_tally: [
  #       steps: [:assemble, :tar]
  #     ]
  #   ]
  # end

  # defp release do
  #   [
  #     overwrite: true,
  #     cookie: "#{@app}_cookie",
  #     quiet: true,
  #     steps: [:assemble, &Bakeware.assemble/1],
  #     strip_beams: Mix.env() == :prod
  #   ]
  # end

  # def releases do
  #   [
  #     # wc_tally: [
  #     steps: [:assemble, &Burrito.wrap/1],
  #     burrito: [
  #       targets: [
  #         windows: [os: :windows, cpu: :x86_64]
  #       ]
  #     ]
  #   ]

  # ]
  # end

  # def releases do
  #   [
  #     [
  #       steps: [:assemble, &Burrito.wrap/1],
  #       burrito: [
  #         targets: [
  #           macos: [os: :darwin, cpu: :x86_64],
  #           linux: [os: :linux, cpu: :x86_64],
  #           windows: [os: :windows, cpu: :x86_64]
  #         ]
  #       ]
  #     ],
  #     [
  #       overwrite: true,
  #       cookie: "#{@app}_cookie",
  #       quiet: true,
  #       steps: [:assemble, &Bakeware.assemble/1],
  #       strip_beams: Mix.env() == :prod
  #     ]
  #   ]
  # end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7.18"},
      {:phoenix_ecto, "~> 4.5"},
      {:ecto_sql, "~> 3.10"},
      {:ecto_sqlite3, "~> 0.15.1"},
      {:phoenix_html, "~> 4.1"},
      {:phoenix_live_reload, "~> 1.2", runtime: Mix.env() == :dev && Mix.target() == :host},
      {:phoenix_live_view, "~> 1.0.0"},
      {:floki, ">= 0.30.0", only: :test},
      {:phoenix_live_dashboard, "~> 0.8.3"},
      {:esbuild, "~> 0.8", runtime: Mix.env() == :dev && Mix.target() == :host},
      {:tailwind, "~> 0.2", runtime: Mix.env() == :dev && Mix.target() == :host},
      {:heroicons,
       github: "tailwindlabs/heroicons",
       tag: "v2.1.1",
       sparse: "optimized",
       app: false,
       compile: false,
       depth: 1},
      {:telemetry_metrics, "~> 1.0"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.26"},
      {:jason, "~> 1.2"},
      {:plug_cowboy, "~> 2.5"},
      {:net_address, "~> 0.2.0"},
      {:ecto_fields, "~> 1.3.0"},
      {:logger_file_backend, "~> 0.0.10"},
      {:codec, "~> 0.1.2"},
      {:binary, "~> 0.0.5"},
      {:struct_access, "~> 1.1.2"},
      {:ecto_sqlite3_extras, "~> 1.2"},
      {:mox, "~> 0.5.2", only: :test},
      {:dns_cluster, "~> 0.1.1"},
      {:bandit, "~> 1.5"},
      {:burrito, "~> 1.2"},
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "assets.setup", "assets.build"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "assets.setup": [
        "tailwind.install --if-missing",
        "esbuild.install --if-missing",
        "cmd --cd assets npm ci"
      ],
      "assets.build": ["tailwind up_tally", "esbuild up_tally"],
      "assets.deploy": [
        "tailwind up_tally --minify",
        "esbuild up_tally --minify",
        "phx.digest"
      ],
      releases: releases()
    ]
  end

  def releases do
    [
      up_tally: [
        steps: [:assemble, &Burrito.wrap/1],
        burrito: [
          targets: [
            macos: [os: :darwin, cpu: :x86_64],
            linux: [os: :linux, cpu: :x86_64],
            windows: [os: :windows, cpu: :x86_64]
          ]
        ]
      ]
    ]
  end
end
