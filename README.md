# UpTally

A utility to connect Blackmagic ATEM switchers to TSL Endpoints using the TSL 3.1 Version protocol.

Written in Elixir using Genservers for the various component to allow for graceful crashing and automatic healing. Using the Phoenix framework for the realtime UI.

Tested on a Blackmagic Constellation HD, but should work for other versions, though code may need adjusting to allow for different input/ME/DSK/Key counts e.t.c as these are not currently dynamicaly detected.

Crosspoint data is pulled from the Atem API, so may get broken by future updates!

## Running in Dev

Clone this Repo and install Elixir for you platform as per the Elixir docs.

To start the Phoenix server:

- Run `mix setup` to install and setup dependencies
- Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Build on Windows

- Install [Chocolatey](https://chocolatey.org/install).
- Install [Elixir](https://elixir-lang.org/install.html).
- Install Git - `choco install git`
- Install Visual Studio 2022
- Install Build Tools (from within Visual Studio)
- Clone this Repo
- Open a Visual Studio command prompt (cmd /K "C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Auxiliary\Build\vcvars32.bat" amd64)
- Set PROD for build `set MIX_ENV=prod`
- C:\ProgramData\chocolatey\lib\Elixir\tools\bin\mix deps.get --only prod
- C:\ProgramData\chocolatey\lib\Elixir\tools\bin\mix setup
- C:\ProgramData\chocolatey\lib\Elixir\tools\bin\mix compile
- C:\ProgramData\chocolatey\lib\Elixir\tools\bin\mix assets.deploy
- C:\ProgramData\chocolatey\lib\Elixir\tools\bin\mix release

## Installing on Windows

- Copy the full up_tally folder located at `_build/prod/release` to the machine you wish to install on. Sensible default `C:\ProgramData\up_tally`.
- Open and elevated command prompt and install as a service by executing (assuming you have used the path above) `C:\ProgramData\up_tally\bin\up_tally install`
- Start the service by issuing the command `C:\ProgramData\up_tally\erts-13.2\bin\erlsrv.exe start up_tally_up_tally`
- UI can now be access in any browser on the computer at [http://localhost:4000](http://localhost:4000) or on any other device using the computers IP address and port 4000

## Starting / Stopping

To stop the service use the commmand `C:\ProgramData\up_tally\erts-13.2\bin\erlsrv.exe stop up_tally_up_tally`

To restart the service use the command `C:\ProgramData\up_tally\erts-13.2\bin\erlsrv.exe start up_tally_up_tally`

## Upgrading

Stop the service `C:\ProgramData\up_tally\erts-13.2\bin\erlsrv.exe stop up_tally_up_tally` and kill the epmd service `tskill epmd`

Replace the files in the up_tally folder with the new release, saying yes to any files it asks to replace. You may get a copy error for the bin folder, this is fine.

Restart the service `C:\ProgramData\up_tally\erts-13.2\bin\erlsrv.exe start up_tally_up_tally`

## Removing the service

`C:\ProgramData\up_tally\erts-13.2\bin\erlsrv.exe remove up_tally_up_tally`

`tskill epmd`

## Building static binaries using Burrito (cross plastform)

- Install 7Z as per the Burrito requirements [Burrito](https://github.com/burrito-elixir/burrito?tab=readme-ov-file#mix-release-config-options)
- Install [ASDF](https://asdf-vm.com/guide/getting-started.html)
- Install [Erlang ASDF Plugin](https://github.com/asdf-vm/asdf-erlang)
- Install [Elixir ASDF Plugin](https://github.com/asdf-vm/asdf-elixir)
- Install [ZIG ASDF Plugin](https://github.com/asdf-community/asdf-zig)
- Install [NodeJS ASDF Plugin](https://github.com/asdf-vm/asdf-nodejs)
- Git Clone this repo to a local folder `git clone https://github.com/up-tally/up_tally.git`
- Change into the downloaded folder `cd up_tally`
- run `asdf install`
- run `mix setup`
- run `mix assets.deploy`
- run `MIX_ENV=prod mix release`
- Binaries will be built and placed under `\burrito_out\` folder
