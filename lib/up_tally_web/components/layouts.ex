defmodule UpTallyWeb.Layouts do
  use UpTallyWeb, :html

  embed_templates "layouts/*"
end
