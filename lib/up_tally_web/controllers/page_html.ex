defmodule UpTallyWeb.PageHTML do
  use UpTallyWeb, :html

  embed_templates "page_html/*"
end
