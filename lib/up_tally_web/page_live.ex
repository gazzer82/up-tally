defmodule UpTallyWeb.PageLive do
  use UpTallyWeb, :live_view

  alias UpTally.Services.StateServer
  alias UpTally.Services

  require Logger

  def mount(_params, _session, socket) do
    if connected?(socket) do
      subscribe()
    end

    {:ok, socket |> set_data()}
  end

  defp subscribe do
    Phoenix.PubSub.subscribe(UpTally.PubSub, "state")
    Phoenix.PubSub.subscribe(UpTally.PubSub, "status")
  end

  defp set_data(socket) do
    socket
    |> assign(
      :channels,
      StateServer.get_inputs() |> Enum.map(fn {_k, v} -> v end)
    )
    |> assign(
      :atem_status,
      StateServer.get_atem_status()
    )
    |> assign(
      :tsl_status,
      Services.get_tsl_status()
    )
  end

  def handle_info({:routing, inputs}, socket) do
    Logger.debug("Updating UI with new channel information")
    {:noreply, assign(socket, :channels, inputs |> Enum.map(fn {_k, v} -> v end))}
  end

  def handle_info({:atem_status, status}, socket) do
    {:noreply, assign(socket, :atem_status, status)}
  end

  def handle_info({:tsl_state, state}, socket) do
    {:noreply, assign(socket, :tsl_status, state)}
  end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp map_status_text(status) when status in [:connected, :transmitting, :recieving] do
    "Connected"
  end

  defp map_status_text(status) do
    status
  end

  defp red_tally(class, on_pgm) when on_pgm == true do
    "#{class} bg-red-700"
  end

  defp red_tally(class, _channel), do: class

  defp green_tally(class, on_pv) when on_pv == true do
    "#{class} bg-green-700"
  end

  defp green_tally(class, _channel), do: class

  defp get_status_class(status) when status in [:connected, :transmitting, :recieving] do
    "inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-green-100 text-green-800 md:mt-2 lg:mt-0"
  end

  defp get_status_class(_status) do
    "inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-red-100 text-red-800 md:mt-2 lg:mt-0"
  end

  defp get_tsl_status_class(%{unknown: unknown, disconnected: disconnected})
       when unknown > 0 or disconnected > 0 do
    "inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-red-100 text-red-800 md:mt-2 lg:mt-0"
  end

  defp get_tsl_status_class(%{connecting: connecting})
       when connecting > 0 do
    "inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-orange-100 text-orange-800 md:mt-2 lg:mt-0"
  end

  defp get_tsl_status_class(_status) do
    "inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium bg-green-100 text-green-800 md:mt-2 lg:mt-0"
  end
end
