defmodule UpTallyWeb.VideohubLive.Index do
  use UpTallyWeb, :live_view

  alias UpTally.Services
  alias UpTally.Services.Videohub

  require Logger

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: Phoenix.PubSub.subscribe(UpTally.PubSub, "videohub")
    {:ok, set_state(socket)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Videohub")
    |> assign(:videohub, Services.get_videohub!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Videohub")
    |> assign(:videohub, %Videohub{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Videohub")
    |> assign(:videohub, nil)
  end

  defp set_state(socket) do
    stream(socket, :videohub_collection, Services.list_videohub_with_status(), reset: true)
  end

  @impl true
  def handle_info({UpTallyWeb.VideohubLive.FormComponent, {:saved, videohub}}, socket) do
    {:noreply, stream_insert(socket, :videohub_collection, videohub)}
  end

  @impl true
  def handle_info({:disconnected, _id}, socket) do
    Logger.debug("Disconnected")
    {:noreply, set_state(socket)}
  end

  @impl true
  def handle_info({:connected, _id}, socket) do
    Logger.debug("Connected")
    {:noreply, set_state(socket)}
  end

  @impl true
  def handle_info(_, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    videohub = Services.get_videohub!(id)
    {:ok, _} = Services.delete_videohub(videohub)

    {:noreply, stream_delete(socket, :videohub_collection, videohub)}
  end
end
