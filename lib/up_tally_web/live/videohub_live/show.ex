defmodule UpTallyWeb.VideohubLive.Show do
  use UpTallyWeb, :live_view

  alias UpTally.Services

  require Logger

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: Phoenix.PubSub.subscribe(UpTally.PubSub, "videohub")
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:videohub, Services.get_videohub!(id))
     |> assign(:state, UpTally.Services.VideohubServer.get_state(String.to_integer(id)))}
  end

  @impl true
  def handle_info({UpTallyWeb.VideohubLive.FormComponent, {:saved, videohub}}, socket) do
    {:noreply,
     socket
     |> assign(:vidohub, videohub)
     |> assign(:state, UpTally.Services.VideohubServer.get_state(videohub.id))}
  end

  @impl true
  def handle_info({:disconnected, update_id}, %{assigns: %{videohub: %{id: id}}} = socket)
      when update_id == id do
    Logger.debug("Disconnected")
    {:noreply, socket |> assign(:state, UpTally.Services.VideohubServer.get_state(id))}
  end

  @impl true
  def handle_info({:connected, update_id}, %{assigns: %{videohub: %{id: id}}} = socket)
      when update_id == id do
    Logger.debug("Connected")
    {:noreply, socket |> assign(:state, UpTally.Services.VideohubServer.get_state(id))}
  end

  @impl true
  def handle_info({:routing, update_id, _}, %{assigns: %{videohub: %{id: id}}} = socket)
      when update_id == id do
    Logger.debug("Updated Routing")
    {:noreply, socket |> assign(:state, UpTally.Services.VideohubServer.get_state(id))}
  end

  @impl true
  def handle_info({:names, update_id, _}, %{assigns: %{videohub: %{id: id}}} = socket)
      when update_id == id do
    Logger.debug("Updated Names")
    {:noreply, socket |> assign(:state, UpTally.Services.VideohubServer.get_state(id))}
  end

  @impl true
  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp page_title(:show), do: "Show Videohub"
  defp page_title(:edit), do: "Edit Videohub"
end
