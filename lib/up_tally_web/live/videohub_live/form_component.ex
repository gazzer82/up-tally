defmodule UpTallyWeb.VideohubLive.FormComponent do
  use UpTallyWeb, :live_component

  alias UpTally.Services

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage videohub records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="videohub-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:ip]} type="text" label="Ip" />
        <.input field={@form[:port]} type="number" label="Port" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Videohub</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{videohub: videohub} = assigns, socket) do
    changeset = Services.change_videohub(videohub)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"videohub" => videohub_params}, socket) do
    changeset =
      socket.assigns.videohub
      |> Services.change_videohub(videohub_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"videohub" => videohub_params}, socket) do
    save_videohub(socket, socket.assigns.action, videohub_params)
  end

  defp save_videohub(socket, :edit, videohub_params) do
    case Services.update_videohub(socket.assigns.videohub, videohub_params) do
      {:ok, videohub} ->
        notify_parent({:saved, videohub})

        {:noreply,
         socket
         |> put_flash(:info, "Videohub updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_videohub(socket, :new, videohub_params) do
    case Services.create_videohub(videohub_params) do
      {:ok, videohub} ->
        notify_parent({:saved, videohub})

        {:noreply,
         socket
         |> put_flash(:info, "Videohub created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
