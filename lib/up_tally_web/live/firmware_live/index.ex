defmodule UpTallyWeb.FirmWareLive.Index do
  use UpTallyWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:uploaded_files, [])
     |> allow_upload(:firmware,
       accept: ~w(.fw),
       max_entries: 1,
       max_file_size: 200_000_000,
       auto_upload: true,
       progress: &handle_progress/3,
       writer: fn _name, _entry, _socket -> {UpTally.FirmwareUploader, []} end
     )}
  end

  @impl true
  def handle_event("validate", _params, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("save", _params, socket) do
    uploaded_files =
      consume_uploaded_entries(socket, :firmware, fn %{path: path}, _entry ->
        {:ok, path}
      end)

    {:noreply, update(socket, :uploaded_files, &(&1 ++ uploaded_files))}
  end

  defp handle_progress(:firmware, entry, socket) do
    if entry.done? do
      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div class="mx-auto max-w-2xl">
      <.form for={%{}} phx-change="validate" phx-submit="save" class="flex flex-col gap-4">
        <div
          class="flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10"
          phx-drop-target={@uploads.firmware.ref}
        >
          <div class="text-center">
            <svg
              class="mx-auto h-12 w-12 text-gray-300"
              viewBox="0 0 24 24"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fill-rule="evenodd"
                d="M1.5 6a2.25 2.25 0 012.25-2.25h16.5A2.25 2.25 0 0122.5 6v12a2.25 2.25 0 01-2.25 2.25H3.75A2.25 2.25 0 011.5 18V6zM3 16.06V18c0 .414.336.75.75.75h16.5A.75.75 0 0021 18v-1.94l-2.69-2.689a1.5 1.5 0 00-2.12 0l-.88.879.97.97a.75.75 0 11-1.06 1.06l-5.16-5.159a1.5 1.5 0 00-2.12 0L3 16.061zm10.125-7.81a1.125 1.125 0 112.25 0 1.125 1.125 0 01-2.25 0z"
                clip-rule="evenodd"
              />
            </svg>
            <div class="mt-4 flex text-sm leading-6 text-gray-600">
              <label
                for={@uploads.firmware.ref}
                class="relative cursor-pointer rounded-md bg-white font-semibold text-indigo-600"
              >
                <span>Upload firmware file</span>
                <.live_file_input upload={@uploads.firmware} class="sr-only" />
              </label>
              <p class="pl-1">or drag and drop</p>
            </div>
            <p class="text-xs leading-5 text-gray-600">Binary or hex files up to 25MB</p>
          </div>
        </div>

        <%= for entry <- @uploads.firmware.entries do %>
          <div class="flex gap-4 items-center">
            <div class="flex-1">
              <div class="text-sm font-semibold"><%= entry.client_name %></div>
              <div class="text-xs text-gray-600">
                <%= entry.progress %>%
              </div>
            </div>
            <button
              type="button"
              phx-click="cancel-upload"
              phx-value-ref={entry.ref}
              class="text-red-500"
            >
              &times;
            </button>
          </div>

          <%= for err <- upload_errors(@uploads.firmware, entry) do %>
            <div class="text-red-500 text-sm"><%= err %></div>
          <% end %>
        <% end %>

        <div>
          <button
            type="submit"
            class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500"
          >
            Upload Firmware
          </button>
        </div>
      </.form>
    </div>
    """
  end
end
