defmodule UpTallyWeb.TslLive.Index do
  use UpTallyWeb, :live_view

  alias UpTally.Services
  alias UpTally.Services.Tsl

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     stream(
       socket,
       :tsl_collection,
       Services.list_tsl()
       |> Enum.map(fn tsl ->
         %{id: tsl.id, config: tsl, status: Services.get_tsl_state(tsl.id)}
       end)
     )}
  end

  # defp set_status(socket) do
  #   Services.list_tsl()
  # end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Tsl")
    |> assign(:tsl, Services.get_tsl!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Tsl")
    |> assign(:tsl, %Tsl{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Tsl Clients")
    |> assign(:tsl, nil)
  end

  @impl true
  def handle_info({UpTallyWeb.TslLive.FormComponent, {:saved, tsl}}, socket) do
    {:noreply,
     stream_insert(socket, :tsl_collection, %{
       id: tsl.id,
       config: tsl,
       status: UpTally.Services.get_tsl_state(tsl.id)
     })}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    tsl = Services.get_tsl!(id)
    {:ok, _} = Services.delete_tsl(tsl)

    {:noreply, stream_delete(socket, :tsl_collection, %{id: tsl.id, config: tsl, status: nil})}
  end
end
