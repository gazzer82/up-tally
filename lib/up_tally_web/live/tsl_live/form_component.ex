defmodule UpTallyWeb.TslLive.FormComponent do
  use UpTallyWeb, :live_component

  alias UpTally.Services

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>
      <.simple_form
        for={@form}
        id="tsl-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:ip]} type="text" label="TSL Transmit IP" />
        <.input field={@form[:port]} type="number" label="TSL Transmit Port" />
        <.input field={@form[:tsl_offset]} type="number" label="TSL ID Offset" />
        <.input field={@form[:me1]} type="checkbox" label="Monitor ME1" />
        <.input field={@form[:me2]} type="checkbox" label="Monitor ME2" />
        <.input field={@form[:me3]} type="checkbox" label="Monitor ME3" />
        <.input field={@form[:me4]} type="checkbox" label="Monitor ME4" />
        <.input
          field={@form[:interface]}
          type="select"
          label="Local Interface IP"
          options={get_interfaces()}
        />
        <.input
          field={@form[:tsl_version]}
          type="select"
          label="Tsl Protocol version"
          options={get_tsl_options()}
        />
        <:actions>
          <.button phx-disable-with="Saving...">Save Tsl</.button>
        </:actions>
        <.input
          field={@form[:send_interval]}
          type="number"
          label="TSL Send delay between each message"
        />
        <.input field={@form[:polling_enabled]} type="checkbox" label="Enable TSL polling send" />
        <.input field={@form[:polling_interval]} type="number" label="Polling interval" />
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{tsl: tsl, action: action} = assigns, socket) when action == :edit do
    changeset = Services.change_update_tsl(tsl)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def update(%{tsl: tsl} = assigns, socket) do
    changeset = Services.change_tsl(tsl)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"tsl" => tsl_params}, socket) do
    changeset =
      socket.assigns.tsl
      |> Services.change_tsl(tsl_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"tsl" => tsl_params}, socket) do
    save_tsl(socket, socket.assigns.action, tsl_params)
  end

  defp save_tsl(socket, :edit, tsl_params) do
    case Services.update_tsl(socket.assigns.tsl, tsl_params) do
      {:ok, tsl} ->
        notify_parent({:saved, tsl})

        {:noreply,
         socket
         |> put_flash(:info, "Tsl updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_tsl(socket, :new, tsl_params) do
    case Services.create_tsl(tsl_params) do
      {:ok, tsl} ->
        notify_parent({:saved, tsl})

        {:noreply,
         socket
         |> put_flash(:info, "Tsl created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})

  defp get_tsl_options() do
    [{"TSL v3.1", :tsl_v3}, {"TSL v5", :tsl_v5}]
  end

  defp get_interfaces() do
    UpTally.Interfaces.list_interfaces() |> Enum.map(&{IP.to_string(&1), IP.to_string(&1)})
  end
end
