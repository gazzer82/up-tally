defmodule UpTallyWeb.AtemLive.FormComponent do
  use UpTallyWeb, :live_component

  alias UpTally.Services

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>

      <.simple_form
        for={@form}
        id="atem-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:ip]} type="text" label="Ip" />
        <.input field={@form[:port]} type="number" label="Port" />
        <.input
          field={@form[:model]}
          type="select"
          options={[
            {"Constellation 2ME", "constellation_2me"},
            {"Constellation 4ME", "constellation_4me"}
          ]}
          label="Model"
        />
        <.input field={@form[:test]} type="checkbox" label="Test Mode" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Atem</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{atem: atem, action: action} = assigns, socket) when action == :edit do
    changeset = Services.change_update_atem(atem)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def update(%{atem: atem} = assigns, socket) do
    changeset = Services.change_atem(atem)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"atem" => atem_params}, socket) do
    changeset =
      socket.assigns.atem
      |> Services.change_atem(atem_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"atem" => atem_params}, socket) do
    save_atem(socket, socket.assigns.action, atem_params)
  end

  defp save_atem(socket, :edit, atem_params) do
    case Services.update_atem(socket.assigns.atem, atem_params) do
      {:ok, atem} ->
        notify_parent({:saved, atem})

        {:noreply,
         socket
         |> put_flash(:info, "Atem updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect(changeset)
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_atem(socket, :new, atem_params) do
    case Services.create_atem(atem_params) do
      {:ok, atem} ->
        notify_parent({:saved, atem})

        {:noreply,
         socket
         |> put_flash(:info, "Atem created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect(changeset)
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
