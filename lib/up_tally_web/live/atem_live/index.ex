defmodule UpTallyWeb.AtemLive.Index do
  use UpTallyWeb, :live_view

  alias UpTally.Services
  alias UpTally.Services.Atem

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :atem_collection, Services.list_atem())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Atem")
    |> assign(:atem, Services.get_atem!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Atem")
    |> assign(:atem, %Atem{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Atem")
    |> assign(:atem, nil)
  end

  @impl true
  def handle_info({UpTallyWeb.AtemLive.FormComponent, {:saved, atem}}, socket) do
    {:noreply, stream_insert(socket, :atem_collection, atem)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    atem = Services.get_atem!(id)
    {:ok, _} = Services.delete_atem(atem)

    {:noreply, stream_delete(socket, :atem_collection, atem)}
  end
end
