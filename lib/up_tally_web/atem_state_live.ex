defmodule UpTallyWeb.AtemStateLive do
  use UpTallyWeb, :live_view

  alias UpTally.Services.AtemServer

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: Phoenix.PubSub.subscribe(UpTally.PubSub, "atem")
    %{id: id} = UpTally.Services.list_atem() |> List.first(%{id: 0})
        atem_state = AtemServer.get_state(id)
        {:ok, socket |> set_state(atem_state)}
  end

  defp set_state(socket, {:ok, state}) do
    json_state = Jason.encode!(state, pretty: true)
    socket |> assign(:state, state) |> assign(:json_state, json_state) |> push_event("atem_state", %{data: state})
  end

  defp set_state(socket, _) do
    socket
  end

  @impl true
  def handle_info({:updated_ui, state}, socket) do
    {:noreply, socket |> set_state(state)}
  end

  @impl true
  def handle_info(_, socket) do
    {:noreply, socket}
  end
end
