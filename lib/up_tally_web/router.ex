defmodule UpTallyWeb.Router do
  use UpTallyWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {UpTallyWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", UpTallyWeb do
    pipe_through :browser

    live "/", PageLive
    live "/state", AtemStateLive

    live "/tsl", TslLive.Index, :index
    live "/tsl/new", TslLive.Index, :new
    live "/tsl/:id/edit", TslLive.Index, :edit

    live "/atem", AtemLive.Index, :index
    live "/atem/new", AtemLive.Index, :new
    live "/atem/:id/edit", AtemLive.Index, :edit

    live "/videohub", VideohubLive.Index, :index
    live "/videohub/new", VideohubLive.Index, :new
    live "/videohub/:id/edit", VideohubLive.Index, :edit

    live "/videohub/:id", VideohubLive.Show, :show
    live "/videohub/:id/show/edit", VideohubLive.Show, :edit

    live "/firmware", FirmWareLive.Index, :index
  end

  import Phoenix.LiveDashboard.Router

  scope "/dev" do
    pipe_through :browser

    live_dashboard "/dashboard", metrics: UpTallyWeb.Telemetry
  end
end
