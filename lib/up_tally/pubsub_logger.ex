defmodule UpTally.PubSubLogger do
  @behaviour :gen_event

  def init(name) do
    # Load configuration
    config = Application.get_env(:logger, name, [])
    config = Keyword.merge(default_config(), config)

    # Validate required settings
    validate_config!(config, name)

    # Prepare formatter function
    {formatter, _format} = Keyword.pop(config, :formatter, {Logger.Formatter, :format})
    format = Keyword.get(config, :format, default_format())
    formatter_fun = compile_formatter(formatter, format)

    # Filter metadata based on config
    metadata = Keyword.get(config, :metadata, [])

    state =
      config
      |> Keyword.put(:formatter, formatter_fun)
      |> Keyword.put(:metadata, metadata)

    {:ok, state}
  end

  def handle_event(
        {level, _gl, {Logger, msg, ts, md}},
        %{level: min_level, metadata_filter: metadata_filter, metadata_reject: metadata_reject} =
          state
      ) do
    # Filter metadata
    level = to_logger_level(level)
    min_level = to_logger_level(min_level)

    if (is_nil(min_level) or Logger.compare_levels(level, min_level) != :lt) and
         metadata_matches?(md, metadata_filter) and
         (is_nil(metadata_reject) or !metadata_matches?(md, metadata_reject)) do
      Phoenix.PubSub.broadcast(
        state[:pubsub],
        state[:topic],
        {:log_message, %{level: level, message: msg, timestamp: ts, metadata: md}}
      )
    else
      {:ok, state}
    end

    # Broadcast via PubSu

    {:ok, state}
  end

  def handle_event(_, state), do: {:ok, state}
  def handle_call(_, state), do: {:ok, :ok, state}
  def handle_info(_, state), do: {:ok, state}

  defp default_config do
    [
      format: "$time $metadata[$level] $message\n",
      formatter: {Logger.Formatter, :format},
      metadata: :all,
      pubsub: UpTally.PubSub,
      topic: "logs"
    ]
  end

  @doc false
  @spec metadata_matches?(Keyword.t(), nil | Keyword.t()) :: true | false
  def metadata_matches?(_md, nil), do: true
  # all of the filter keys are present
  def metadata_matches?(_md, []), do: true

  def metadata_matches?(md, [{key, [_ | _] = val} | rest]) do
    case Keyword.fetch(md, key) do
      {:ok, md_val} ->
        md_val in val && metadata_matches?(md, rest)

      # fail on first mismatch
      _ ->
        false
    end
  end

  def metadata_matches?(md, [{key, val} | rest]) do
    case Keyword.fetch(md, key) do
      {:ok, ^val} ->
        metadata_matches?(md, rest)

      # fail on first mismatch
      _ ->
        false
    end
  end

  defp default_format, do: "$time $metadata[$level] $message\n"

  defp compile_formatter({mod, fun}, format) do
    fn level, message, timestamp, metadata ->
      apply(mod, fun, [level, message, timestamp, metadata, format])
    end
  end

  defp validate_config!(config, name) do
    unless Keyword.get(config, :pubsub) do
      raise ArgumentError, "missing :pubsub configuration for #{inspect(name)}"
    end

    unless Keyword.get(config, :topic) do
      raise ArgumentError, "missing :topic configuration for #{inspect(name)}"
    end
  end

  defp to_logger_level(:warn) do
    if Version.compare(System.version(), "1.11.0") != :lt,
      do: :warning,
      else: :warn
  end

  defp to_logger_level(level), do: level
end
