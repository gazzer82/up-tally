defmodule UpTally.Repo do
  use Ecto.Repo,
    otp_app: :up_tally,
    adapter: Ecto.Adapters.SQLite3
end
