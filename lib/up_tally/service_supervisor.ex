defmodule UpTally.ServicesSupervisor do
  use Supervisor
  require Logger

  def start_link(_args) do
    Logger.notice("Starting the services supervisor")
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    children = [
      UpTally.Services.StateServer
    ]

    children =
      case Application.get_env(:up_tally, :env) do
        :test ->
          Logger.debug("Running supervisor in test mode, not starting genservers by default.")
          children

        _ ->
          children ++
            [
              UpTally.Services.TslSupervisor,
              UpTally.Services.AtemSupervisor,
              UpTally.Services.VideohubSupervisor,
              UpTally.Services.TslController,
              UpTally.Services.AtemController,
              UpTally.Services.VideohubController
            ]
      end

    # children = [UpTally.Services.AtemServer]
    Supervisor.init(children, strategy: :one_for_one)
  end
end
