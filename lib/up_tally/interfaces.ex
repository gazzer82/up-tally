defmodule UpTally.Interfaces do
  def is_valid?(address) do
    ifs = list_interfaces()

    with {:ok, ip} <- IP.from_string(address),
         found when found == true <- Enum.member?(ifs, ip) do
      true
    else
      _ ->
        false
    end
  end

  def list_interfaces do
    :net.getifaddrs(:inet)
    |> (fn {:ok, if_list} -> if_list end).()
    |> Enum.map(& &1.addr.addr)
  end

  def get_default_interface_string() do
    get_default_interface() |> IP.to_string()
  end

  def get_default_interface() do
    cond do
      is_valid?("192.168.10.25") ->
        {192, 168, 10, 25}

      is_valid?("192.168.10.10") ->
        {192, 168, 10, 10}

      true ->
        {127, 0, 0, 1}
    end
  end
end
