defmodule UpTally.Atem.AtemParser do
  require Logger

  alias UpTally.Atem.Parser.Input
  alias UpTally.Atem.Parser.ProgramInput
  alias UpTally.Atem.Parser.PreviewInput
  alias UpTally.Atem.Parser.Transition
  alias UpTally.Atem.Parser.UpstreamKey
  alias UpTally.Atem.Parser.DownstreamKey
  alias UpTally.Atem.Parser.SuperSource

  def parse_packet(packet) do
    Input.parse(packet)
    ProgramInput.parse(packet)
    PreviewInput.parse(packet)
    Transition.parse(packet)
    UpstreamKey.parse(packet)
    DownstreamKey.parse(packet)
    SuperSource.parse(packet)
  end
end
