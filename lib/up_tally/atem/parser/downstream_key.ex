defmodule UpTally.Atem.Parser.DownstreamKey do
  require Bitwise

  require Logger

  import UpTally.Atem.Parser.Utils

  defmodule Key do
    use StructAccess
    @derive Jason.Encoder
    @type t :: %__MODULE__{
            on_air: boolean(),
            is_transitioning: boolean(),
            is_auto_transitioning: boolean(),
            fill_source: integer(),
            key_source: integer(),
            tie: boolean()
          }

    defstruct on_air: false,
              is_transitioning: false,
              is_auto_transitioning: false,
              fill_source: 0,
              key_source: 0,
              tie: false
  end

  def parse(binary)

  def parse(data) do
    parse_downstream_key_on_air(data)
    parse_downstream_key(data)
    parse_downstream_key_properties(data)
  end

  defp parse_downstream_key_on_air(binary)

  defp parse_downstream_key_on_air(<<>>) do
  end

  defp parse_downstream_key_on_air(<<
         "DskS",
         key_index::unsigned-integer-size(8),
         on_air::unsigned-integer-size(8),
         is_transitioning::unsigned-integer-size(8),
         is_auto_transitioning::unsigned-integer-size(8),
         _frames_remaining::unsigned-integer-size(8),
         rest::binary
       >>) do
    dsk = map_dsk(key_index)

    data = %Key{
      on_air: on_air == 1,
      is_transitioning: is_transitioning == 1,
      is_auto_transitioning: is_auto_transitioning == 1
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:downstream_key_on_air, {dsk, data}}
    )

    parse_downstream_key_on_air(rest)
  end

  defp parse_downstream_key_on_air(<<_byte::size(8), rest::binary>>) do
    parse_downstream_key_on_air(rest)
  end

  defp parse_downstream_key(binary)

  defp parse_downstream_key(<<>>) do
  end

  defp parse_downstream_key(<<
         "DskB",
         key_index::unsigned-integer-size(8),
         _::unsigned-integer-size(8),
         fill_source::unsigned-integer-size(16),
         key_source::unsigned-integer-size(16),
         rest::binary
       >>) do
    dsk = map_dsk(key_index)

    data = %Key{
      fill_source: fill_source,
      key_source: key_source
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:downstream_key, {dsk, data}}
    )

    parse_downstream_key(rest)
  end

  defp parse_downstream_key(<<_byte::size(8), rest::binary>>) do
    parse_downstream_key(rest)
  end

  defp parse_downstream_key_properties(binary)

  defp parse_downstream_key_properties(<<>>) do
  end

  defp parse_downstream_key_properties(<<
         "DskP",
         key_index::unsigned-integer-size(8),
         tie::unsigned-integer-size(8),
         rest::binary
       >>) do
    data = %Key{
      tie: tie == 1
    }

    dsk = map_dsk(key_index)

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:downstream_key_properties, {dsk, data}}
    )

    parse_downstream_key_properties(rest)
  end

  defp parse_downstream_key_properties(<<_byte::size(8), rest::binary>>) do
    parse_downstream_key_properties(rest)
  end
end
