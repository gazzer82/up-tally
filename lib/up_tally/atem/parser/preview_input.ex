defmodule UpTally.Atem.Parser.PreviewInput do
  import UpTally.Atem.Parser.Utils

  def parse(binary)

  def parse(<<>>) do
  end

  def parse(
        <<"PrvI", me::unsigned-integer-size(8), _::unsigned-integer-size(8),
          video_source::unsigned-integer-size(16), rest::binary>>
      ) do
    preview_input = %{
      me: map_me(me),
      video_source: video_source
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:pv_input, preview_input}
    )

    parse(rest)
  end

  def parse(<<_byte::size(8), rest::binary>>) do
    parse(rest)
  end
end
