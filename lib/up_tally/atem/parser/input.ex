defmodule UpTally.Atem.Parser.Input do
  import UpTally.Atem.Parser.Utils

  defmodule Input do
    @typedoc """
        Type that represents the state for the atem service
    """
    @derive Jason.Encoder

    @type t :: %__MODULE__{
            video_src: integer(),
            long_name: String.t(),
            short_name: String.t(),
            are_names_default: boolean(),
            # external_ports: external_port_types,
            external_port_type: atom(),
            internal_port_type: atom(),
            source_availability: integer(),
            me_availability: integer()
          }
    defstruct connected: false,
              video_src: nil,
              long_name: "",
              short_name: "",
              are_names_default: true,
              external_port_type: nil,
              internal_port_type: nil,
              source_availability: nil,
              me_availability: nil
  end

  def parse(binary, data \\ [])

  def parse(<<>>, data) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:inputs, data}
    )

    data
  end

  def parse(
        <<"InPr", video_src::unsigned-integer-size(16), long_name::binary-size(20),
          short_name::binary-size(4), are_names_default::unsigned-integer-size(8),
          _external_port_types::unsigned-integer-size(16),
          external_port_type::unsigned-integer-size(16),
          internal_port_type::unsigned-integer-size(8),
          source_availability::unsigned-integer-size(8),
          me_availability::unsigned-integer-size(8), rest::binary>>,
        data
      ) do
    # video_src = <<video_src::unsigned-integer-size(16)>>

    input = %Input{
      video_src: video_src,
      long_name: split_at_null(long_name),
      short_name: split_at_null(short_name),
      are_names_default: are_names_default == 1,
      # external_ports: external_port_types,
      external_port_type: map_external_port_types(external_port_type),
      internal_port_type: map_internal_port_types(internal_port_type),
      source_availability: source_availability,
      me_availability: me_availability
    }

    parse(rest, data ++ [input])
  end

  def parse(<<_byte::size(8), rest::binary>>, processed) do
    parse(rest, processed)
  end

  defp map_external_port_types(port_type) do
    case port_type do
      0 -> :Internal
      1 -> :SDI
      2 -> :HDMI
      3 -> :Composite
      4 -> :Component
      5 -> :SVideo
      _ -> :Unknown
    end
  end

  defp map_internal_port_types(port_type) do
    case port_type do
      0 -> :External
      1 -> :Black
      2 -> :Color_Bars
      3 -> :Color_Generator
      4 -> :Media_Player_Fill
      5 -> :Media_Player_Key
      6 -> :Super_Source
      7 -> :ME_Output
      8 -> :Auxilary
      9 -> :Mask
      _ -> :Unknown
    end
  end
end
