defmodule UpTally.Atem.Parser.Utils do
  def split_at_null(binary) do
    [first | _rest] = binary |> Binary.to_list() |> Enum.chunk_by(&(&1 == 0))
    to_string(first)
  end

  def map_me(integer)

  def map_me(me) when me == 0, do: :me1

  def map_me(me) when me == 1, do: :me2

  def map_me(me) when me == 2, do: :me3

  def map_me(me) when me == 3, do: :me4

  def map_me(_me), do: :unknown

  def map_dsk(integer)

  def map_dsk(dsk) when dsk == 0, do: :dsk1

  def map_dsk(dsk) when dsk == 1, do: :dsk2

  def map_dsk(dsk) when dsk == 2, do: :dsk3

  def map_dsk(dsk) when dsk == 3, do: :dsk4

  def map_dsk(_dsk), do: :unknown

  def map_key(integer)

  def map_key(key) when key == 0, do: :key_1

  def map_key(key) when key == 1, do: :key_2

  def map_key(key) when key == 2, do: :key_3

  def map_key(key) when key == 3, do: :key_4

  def map_key(_key), do: :unknown
end
