defmodule UpTally.Atem.Parser.ProgramInput do
  import UpTally.Atem.Parser.Utils

  def parse(binary)

  def parse(<<>>) do
  end

  def parse(
        <<"PrgI", me::unsigned-integer-size(8), _::unsigned-integer-size(8),
          video_source::unsigned-integer-size(16), rest::binary>>
      ) do
    program_input = %{
      me: map_me(me),
      video_source: video_source
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:pgm_input, program_input}
    )

    parse(rest)
  end

  def parse(<<_byte::size(8), rest::binary>>) do
    parse(rest)
  end
end
