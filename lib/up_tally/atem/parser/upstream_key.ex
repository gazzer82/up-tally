defmodule UpTally.Atem.Parser.UpstreamKey do
  require Bitwise

  import UpTally.Atem.Parser.Utils

  defmodule Key do
    use StructAccess
    @derive Jason.Encoder
    @type t :: %__MODULE__{
            key_type: atom(),
            fill_source: integer(),
            key_source: integer(),
            on_air: boolean()
          }

    defstruct key_type: :none,
              fill_source: 0,
              key_source: 0,
              on_air: false
  end

  def parse(binary)

  def parse(data) do
    parse_upstream_key_on_air(data)
    parse_upstream_key(data)
  end

  defp parse_upstream_key_on_air(binary)

  defp parse_upstream_key_on_air(<<>>) do
  end

  defp parse_upstream_key_on_air(
         <<"KeOn", me::unsigned-integer-size(8), key_index::unsigned-integer-size(8),
           on_air::unsigned-integer-size(8), rest::binary>>
       ) do
    me = map_me(me)
    key = map_key(key_index)

    data = %{
      on_air: on_air == 1
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:upstream_key_on_air, {me, key, data}}
    )

    parse_upstream_key_on_air(rest)
  end

  defp parse_upstream_key_on_air(<<_byte::size(8), rest::binary>>) do
    parse_upstream_key_on_air(rest)
  end

  defp parse_upstream_key(binary)

  defp parse_upstream_key(<<>>) do
  end

  defp parse_upstream_key(<<
         "KeBP",
         me::unsigned-integer-size(8),
         key_index::unsigned-integer-size(8),
         key_type::unsigned-integer-size(8),
         _::unsigned-integer-size(8),
         _can_fly::unsigned-integer-size(8),
         _fly_enabled::unsigned-integer-size(8),
         fill_source::unsigned-integer-size(16),
         #  _::binary-size(1),
         key_source::unsigned-integer-size(16),
         rest::binary
       >>) do
    me = map_me(me)
    key = map_key(key_index)

    data = %Key{
      key_type: map_key_type(key_type),
      fill_source: fill_source,
      key_source: key_source
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:upstream_key, {me, key, data}}
    )

    parse_upstream_key(rest)
  end

  defp parse_upstream_key(<<_byte::size(8), rest::binary>>) do
    parse_upstream_key(rest)
  end

  defp map_key_type(index) do
    case index do
      0 -> :luma
      1 -> :chroma
      2 -> :pattern
      3 -> :dve
    end
  end
end
