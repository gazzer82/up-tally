defmodule UpTally.Atem.Parser.Transition do
  import UpTally.Atem.Parser.Utils

  @bg 1
  @key_1 2
  @key_2 4
  @key_3 8
  @key_4 16

  require Bitwise

  defmodule Transition do
    @moduledoc """
    Represents the transition state for ATEM video mixer transitions.
    Tracks which video layers are included in the transition including background and keys 1-4.
    """

    use StructAccess
    @derive Jason.Encoder

    @doc """
    Transition struct containing boolean flags for background and key layers.

    Fields:
    - bg: When true, includes background layer in transition
    - key_1: When true, includes key 1 layer in transition
    - key_2: When true, includes key 2 layer in transition
    - key_3: When true, includes key 3 layer in transition
    - key_4: When true, includes key 4 layer in transition
    """
    @type t :: %__MODULE__{
            bg: boolean(),
            key_1: boolean(),
            key_2: boolean(),
            key_3: boolean(),
            key_4: boolean()
          }

    defstruct bg: false,
              key_1: false,
              key_2: false,
              key_3: false,
              key_4: false
  end

  defmodule Settings do
    use StructAccess
    @derive Jason.Encoder

    @type t :: %__MODULE__{
            style: atom(),
            style_next: atom(),
            transition: UpTally.Atem.Parser.Transition.Transition.t(),
            transition_next: UpTally.Atem.Parser.Transition.Transition.t(),
            dip_src: integer(),
            wipe_border_width: integer(),
            wipe_border_src: integer(),
            dve_fill_src: integer(),
            dve_key_enabled: boolean(),
            dve_effect_type: atom(),
            dve_key_src: integer()
          }

    defstruct style: :none,
              style_next: :none,
              transition: %UpTally.Atem.Parser.Transition.Transition{},
              transition_next: %UpTally.Atem.Parser.Transition.Transition{},
              dip_src: 0,
              wipe_border_width: 0,
              wipe_border_src: 0,
              dve_fill_src: 0,
              dve_key_enabled: false,
              dve_effect_type: :none,
              dve_key_src: 0
  end

  def parse(binary)

  def parse(data) do
    parse_transition_settings(data)
    parse_active_transition(data)
    parse_transition_dip(data)
    parse_transition_wipe(data)
    parse_transition_dve(data)
  end

  defp parse_transition_settings(binary)

  defp parse_transition_settings(<<>>) do
  end

  defp parse_transition_settings(
         <<"TrSS", me::unsigned-integer-size(8), style::unsigned-integer-size(8),
           transition::unsigned-integer-size(8), style_next::unsigned-integer-size(8),
           transition_next::unsigned-integer-size(8), rest::binary>>
       ) do
    me = map_me(me)

    settings = %Settings{
      style: map_style(style),
      style_next: map_style(style_next),
      transition: map_transition(transition),
      transition_next: map_transition(transition_next)
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:transition_settings, {me, settings}}
    )

    parse_transition_settings(rest)
  end

  defp parse_transition_settings(<<_byte::size(8), rest::binary>>) do
    parse_transition_settings(rest)
  end

  defp map_style(style) do
    case style do
      0 ->
        :mix

      1 ->
        :dip

      2 ->
        :wipe

      3 ->
        :dve

      4 ->
        :sting
    end
  end

  defp map_transition(transition) do
    %Transition{
      bg: Bitwise.band(@bg, transition) == @bg,
      key_1: Bitwise.band(@key_1, transition) == @key_1,
      key_2: Bitwise.band(@key_2, transition) == @key_2,
      key_3: Bitwise.band(@key_3, transition) == @key_3,
      key_4: Bitwise.band(@key_4, transition) == @key_4
    }
  end

  defp parse_active_transition(binary)

  defp parse_active_transition(<<>>) do
  end

  defp parse_active_transition(
         <<"TrPs", me::unsigned-integer-size(8), in_transition::unsigned-integer-size(8),
           _frames_remaining::unsigned-integer-size(8), _::unsigned-integer-size(8),
           _position::unsigned-integer-size(16), _::unsigned-integer-size(8), rest::binary>>
       ) do
    transition = %{
      me: map_me(me),
      in_transition: in_transition == 1
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:transition, transition}
    )

    parse_active_transition(rest)
  end

  defp parse_active_transition(<<_byte::size(8), rest::binary>>) do
    parse_active_transition(rest)
  end

  defp parse_transition_dip(binary)

  defp parse_transition_dip(<<>>) do
  end

  defp parse_transition_dip(
         <<"TDpP", me::unsigned-integer-size(8), _rate::unsigned-integer-size(8),
           dip_src::unsigned-integer-size(16), rest::binary>>
       ) do
    me = map_me(me)

    settings = %Settings{
      dip_src: dip_src
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:transition_dip, {me, settings}}
    )

    parse_transition_dip(rest)
  end

  defp parse_transition_dip(<<_byte::size(8), rest::binary>>) do
    parse_transition_dip(rest)
  end

  defp parse_transition_wipe(binary)

  defp parse_transition_wipe(<<>>) do
  end

  defp parse_transition_wipe(
         <<"TWpP", me::unsigned-integer-size(8), _rate::unsigned-integer-size(8),
           _pattern::unsigned-integer-size(8), _::unsigned-integer-size(8),
           wipe_border_width::unsigned-integer-size(16),
           wipe_border_src::unsigned-integer-size(16), rest::binary>>
       ) do
    me = map_me(me)

    settings = %Settings{
      wipe_border_width: wipe_border_width,
      wipe_border_src: wipe_border_src
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:transition_wipe, {me, settings}}
    )

    parse_transition_wipe(rest)
  end

  defp parse_transition_wipe(<<_byte::size(8), rest::binary>>) do
    parse_transition_wipe(rest)
  end

  defp parse_transition_dve(binary)

  defp parse_transition_dve(<<>>) do
  end

  defp parse_transition_dve(
         <<"TDvP", me::unsigned-integer-size(8), _rate::unsigned-integer-size(8),
           _::unsigned-integer-size(8), effect_type::unsigned-integer-size(8),
           dve_fill_source::unsigned-integer-size(16), dve_key_source::unsigned-integer-size(16),
           dve_key_enabled::unsigned-integer-size(8), rest::binary>>
       ) do
    me = map_me(me)

    settings = %Settings{
      dve_fill_src: dve_fill_source,
      dve_key_enabled: dve_key_enabled == 1,
      dve_key_src: dve_key_source,
      dve_effect_type: parse_dve_effect_type(effect_type)
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:transition_dve, {me, settings}}
    )

    parse_transition_dve(rest)
  end

  defp parse_transition_dve(<<_byte::size(8), rest::binary>>) do
    parse_transition_dve(rest)
  end

  defp parse_dve_effect_type(effect_index) do
    case effect_index do
      32 ->
        :GraphicCWSpin

      33 ->
        :GraphicCCWSpin

      34 ->
        :GraphicLogoWipe

      _ ->
        :none
    end
  end

  # dve
end
