defmodule UpTally.Atem.Parser.SuperSource do
  defmodule Box do
    use StructAccess
    @derive Jason.Encoder

    @type t :: %__MODULE__{
            enabled: boolean(),
            source: integer()
          }

    defstruct enabled: false,
              source: 0
  end

  defmodule SuperSource do
    use StructAccess
    @derive Jason.Encoder

    @type t :: %__MODULE__{
            fill_source: integer(),
            key_source: integer(),
            foreground_background: atom(),
            box1: UpTally.Atem.Parser.SuperSource.Box.t(),
            box2: UpTally.Atem.Parser.SuperSource.Box.t(),
            box3: UpTally.Atem.Parser.SuperSource.Box.t(),
            box4: UpTally.Atem.Parser.SuperSource.Box.t()
          }
    defstruct fill_source: 0,
              key_source: 0,
              foreground_background: :background,
              box1: %UpTally.Atem.Parser.SuperSource.Box{},
              box2: %UpTally.Atem.Parser.SuperSource.Box{},
              box3: %UpTally.Atem.Parser.SuperSource.Box{},
              box4: %UpTally.Atem.Parser.SuperSource.Box{}
  end

  def parse(binary)

  def parse(message) do
    parse_supersource_box(message)
    parse_supersource_art(message)
  end

  def parse_supersource_box(binary)

  def parse_supersource_box(<<>>) do
  end

  def parse_supersource_box(
        <<"SSBP", _::binary-size(1), box_id::unsigned-integer-size(8),
          enabled::unsigned-integer-size(8), _::binary-size(1), source::unsigned-integer-size(16),
          rest::binary>>
      ) do
    if box_id in 0..3 do
      box = parse_box(box_id)

      settings = %Box{
        enabled: enabled == 1,
        source: source
      }

      Phoenix.PubSub.broadcast(
        UpTally.PubSub,
        "atem_state",
        {:supersource_box, {:ss1, box, settings}}
      )
    end

    parse_supersource_box(rest)
  end

  def parse_supersource_box(<<_byte::size(8), rest::binary>>) do
    parse_supersource_box(rest)
  end

  def parse_supersource_art(binary)

  def parse_supersource_art(<<>>) do
  end

  def parse_supersource_art(
        <<"SSrc", _ss_id::unsigned-integer-size(8), _::unsigned-integer-size(8),
          fill_source::unsigned-integer-size(16), key_source::unsigned-integer-size(16),
          foreground_background::unsigned-integer-size(8), rest::binary>>
      ) do
    settings = %SuperSource{
      fill_source: fill_source,
      key_source: key_source,
      foreground_background: parse_foreground_background(foreground_background)
    }

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem_state",
      {:supersource_art, {:ss1, settings}}
    )

    parse_supersource_art(rest)
  end

  def parse_supersource_art(<<_byte::size(8), rest::binary>>) do
    parse_supersource_art(rest)
  end

  defp parse_foreground_background(option) when option == 0, do: :background

  defp parse_foreground_background(_option), do: :foreground

  defp parse_box(box_id) do
    case box_id do
      0 ->
        :box1

      1 ->
        :box2

      2 ->
        :box3

      3 ->
        :box4
    end
  end
end
