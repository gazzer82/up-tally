defmodule UpTally.Atem.Parser.TallyBySource do
  def parse(binary, data \\ [])

  def parse(<<>>, data) do
    data
  end

  def parse(
        <<"TlSr", sources::unsigned-integer-size(16), rest::binary>>,
        data
      ) do
    # video_src = <<video_src::unsigned-integer-size(16)>>
    <<source_slice::binary-size(sources * 3), _rest::binary>> = rest
    sources = parse_input(source_slice)
    # input = %{
    #   video_src: video_src,
    #   long_name: split_at_null(long_name),
    #   short_name: split_at_null(short_name),
    #   are_names_default: are_names_default,
    #   # external_ports: external_port_types,
    #   external_port_type: map_external_port_types(external_port_type),
    #   internal_port_type: map_internal_port_types(internal_port_type),
    #   source_availability: source_availability,
    #   me_availability: me_availability
    # }

    parse(rest, data ++ [sources])
  end

  def parse(<<_byte::size(8), rest::binary>>, processed) do
    parse(rest, processed)
  end

  defp parse_input(binary, sources \\ [])

  defp parse_input(<<>>, sources) do
    sources
  end

  defp parse_input(
         <<video_src::unsigned-integer-size(16), tally_flags::unsigned-integer-size(8),
           rest::binary>>,
         sources
       ) do
    source = %{video_src: video_src, tally_flags: tally_flags}
    parse_input(rest, sources ++ [source])
  end
end

# for (let i = 0; i < sourceCount; i++) {
#     const source = rawCommand.readUInt16BE(2 + i * 3);
#     const value = rawCommand.readUInt8(4 + i * 3);
#     sources[source] = {
#         program: (value & 0x01) > 0,
#         preview: (value & 0x02) > 0,
#     };
# }
