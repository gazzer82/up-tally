defmodule UpTally.Atem.MixEffect do
  @typedoc """
      Type that represents the state for an ME
  """
  use StructAccess
  @derive Jason.Encoder

  @type t :: %__MODULE__{
          program: integer(),
          preview: integer(),
          in_transition: boolean(),
          transition_settings: UpTally.Atem.Parser.Transition.Settings.t(),
          key_1: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_2: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_3: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_4: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_5: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_6: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_7: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_8: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_9: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_10: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_11: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_12: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_13: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_14: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_15: UpTally.Atem.Parser.UpstreamKey.Key.t(),
          key_16: UpTally.Atem.Parser.UpstreamKey.Key.t()
        }
  defstruct program: 0,
            preview: 0,
            in_transition: false,
            transition_settings: %UpTally.Atem.Parser.Transition.Settings{},
            key_1: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_2: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_3: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_4: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_5: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_6: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_7: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_8: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_9: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_10: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_11: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_12: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_13: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_14: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_15: %UpTally.Atem.Parser.UpstreamKey.Key{},
            key_16: %UpTally.Atem.Parser.UpstreamKey.Key{}
end
