defmodule UpTally.Atem.AtemPacket do
  # @flags_size 5
  # @packet_length_size 11
  # @session_size 16
  # @ack_number_size 16
  # @data_size 16
  # @remote_seq_size 16
  # @local_seq_size 16

  import Bitwise

  def decode(packet) do
    # byte = Binary.first(packet)
    # decoded = byte >>> 3
    # decoded

    %{
      flag: get_flag(packet),
      session: get_session(packet),
      local_seq: get_seq(packet)
    }
  end

  def need_ack(packet) do
    :binary.bin_to_list(packet)
    |> List.first()
    |> Integer.to_string(2)
    |> String.graphemes()
    |> List.first()
    |> (&(&1 == "1")).()
  end

  def get_flag(packet) do
    Binary.first(packet) >>> 3
  end

  def get_session(packet) do
    <<value::unsigned-integer-size(16)>> = :binary.part(packet, 2, 2)
    value
  end

  def get_seq(packet) do
    <<value::unsigned-integer-size(16)>> = :binary.part(packet, 10, 2)
    value
  end
end

# defmodule UpTally.Atem.AtemPacket do
#   use Codec.Generator

#   make_encoder_decoder do
#     <<
#       flags::5,
#       packet_length::11,
#       session::16,
#       acknowledgement_number::16,
#       unknown::16,
#       remote_seq_number::16,
#       local_seq_number::16,
#       payload::binary
#     >>
#   end
# end
