defmodule UpTally.FirmwareUploader do
  @moduledoc false
  @behaviour Phoenix.LiveView.UploadWriter

  @fwup_args ["--apply", "--no-unmount", "-d", "/dev/mmcblk0", "--task", "upgrade"]

  @impl true
  def init(opts) do
    IO.inspect(opts)
    node = :erlang.node()
    parent = :erlang.node()

    {:ok, pid} =
      :rpc.call(node, GenServer, :start, [Fwup.Stream, [cm: parent, fwup_args: @fwup_args]])

    {:ok, %{total: 0, node: node, pid: pid}}
  end

  @impl true
  def meta(_state) do
    %{}
  end

  @impl true
  def write_chunk(chunk, state) do
    :rpc.call(state.node, Fwup.Stream, :send_chunk, [state.pid, chunk])
    {:ok, %{state | total: state.total + byte_size(chunk)}}
  end

  @impl true
  def close(state, _reason) do
    {:ok, state}
  end
end
