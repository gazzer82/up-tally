defmodule UpTally.Services.VideohubControllerBehaviour do
  @callback config_updated() :: nil
  @callback get_status() :: Struct
end
