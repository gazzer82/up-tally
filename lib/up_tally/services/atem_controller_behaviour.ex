defmodule UpTally.Services.AtemControllerBehaviour do
  @callback config_updated() :: nil
  @callback get_status() :: Struct
end
