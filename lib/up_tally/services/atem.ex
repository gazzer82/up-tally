defmodule UpTally.Services.Atem do
  use Ecto.Schema
  import Ecto.Changeset

  schema "atem" do
    field :ip, :string
    field :port, :integer
    field :test, :boolean
    field :model, :string
    timestamps()
  end

  @doc false
  def changeset(atem, attrs) do
    atem
    |> cast(attrs, [:ip, :port, :test, :model])
    |> validate_required([:ip, :port, :test, :model])
  end
end
