defmodule UpTally.Services.Videohub do
  use Ecto.Schema
  import Ecto.Changeset

  schema "videohub" do
    field :port, :integer
    field :ip, :string
    field :name, :string
    timestamps()
  end

  @doc false
  def changeset(videohub, attrs) do
    videohub
    |> cast(attrs, [:ip, :port, :name])
    |> validate_required([:ip, :port, :name])
  end
end
