defmodule UpTally.Services.AtemServer do
  use GenServer
  @name __MODULE__

  @registry :atem_registry

  # @initial_session 0x53AB

  # @initial_session 0x4660

  @hello <<0x10, 0x14, 0x53, 0xAB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3A, 0x00, 0x00, 0x01, 0x00,
           0x00, 0x00, 0x00, 0x00, 0x00, 0x00>>

  @hello_ack <<0x80, 0x0C, 0x53, 0xAB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00>>

  @ack <<0x80, 0x0C, 0x53, 0xAB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00>>

  @maximum_handshake_packets 100

  @timeout 4

  require Logger

  alias UpTally.Atem.AtemParser

  defmodule State do
    @typedoc """
        Type that represents the state for the atem service
    """

    use StructAccess

    @derive {Jason.Encoder,
             only: [
               :connected,
               :me1,
               :me2,
               :me3,
               :me4,
               :dsk1,
               :dsk2,
               :dsk3,
               :dsk4,
               :hydrated,
               :ss1,
               :ss2
             ]}
    @type t :: %__MODULE__{
            connected: boolean(),
            ip: IP.addr(),
            port: integer(),
            model: atom(),
            socket: :socket,
            status: atom(),
            initial_session_id: integer(),
            session_id: integer(),
            remote_packet_id: integer(),
            local_packet_id: integer(),
            alive_timer: reference(),
            last_packet_time: DateTime.t(),
            retry_count: integer(),
            handshake_packet_count: integer(),
            me1: UpTally.Atem.MixEffect.t(),
            me2: UpTally.Atem.MixEffect.t(),
            me3: UpTally.Atem.MixEffect.t(),
            me4: UpTally.Atem.MixEffect.t(),
            dsk1: UpTally.Atem.Parser.DownstreamKey.Key.t(),
            dsk2: UpTally.Atem.Parser.DownstreamKey.Key.t(),
            dsk3: UpTally.Atem.Parser.DownstreamKey.Key.t(),
            dsk4: UpTally.Atem.Parser.DownstreamKey.Key.t(),
            inputs: %{},
            hydrated: %{},
            ss1: UpTally.Atem.Parser.SuperSource.SuperSource.t(),
            ss2: UpTally.Atem.Parser.SuperSource.SuperSource.t()
          }
    defstruct connected: false,
              ip: nil,
              port: nil,
              model: nil,
              socket: nil,
              status: :disconnected,
              initial_session_id: 0x53AB,
              session_id: 0x53AB,
              remote_packet_id: 0,
              local_packet_id: 0,
              alive_timer: nil,
              last_packet_time: 0,
              retry_count: 0,
              handshake_packet_count: 0,
              me1: %UpTally.Atem.MixEffect{},
              me2: %UpTally.Atem.MixEffect{},
              me3: %UpTally.Atem.MixEffect{},
              me4: %UpTally.Atem.MixEffect{},
              dsk1: %UpTally.Atem.Parser.DownstreamKey.Key{},
              dsk2: %UpTally.Atem.Parser.DownstreamKey.Key{},
              dsk3: %UpTally.Atem.Parser.DownstreamKey.Key{},
              dsk4: %UpTally.Atem.Parser.DownstreamKey.Key{},
              inputs: %{},
              hydrated: %{
                inputs: false,
                pgm_input: false,
                pv_input: false,
                transition: false,
                transition_settings: false,
                upstream_key: false,
                upstream_key_on_air: false,
                downstream_key: false,
                downstream_key_on_air: false,
                downstream_key_properties: false,
                transition_dip: false,
                transition_wipe: false,
                transition_dve: false,
                supersource_box: false,
                supersource_art: false
              },
              ss1: %UpTally.Atem.Parser.SuperSource.SuperSource{},
              ss2: %UpTally.Atem.Parser.SuperSource.SuperSource{}
  end

  def start_link(atem_config) do
    name = via_tuple(atem_config.id)
    IO.inspect(@name)
    IO.inspect(name)

    GenServer.start_link(
      @name,
      %State{
        ip: IP.from_string!(atem_config.ip),
        port: atem_config.port,
        model: String.to_atom(atem_config.model)
      },
      name: name
    )
  end

  def reconnect(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :start)
    end
  end

  def update_config(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :update_config)
    end
  end

  def get_state(id) do
    # IO.inspect(id)
    # IO.inspect(Registry.lookup(@registry, id))

    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :get_state)
    end

    # id |> via_tuple() |> GenServer.call(:get_state)
  end

  def is_hydrated?(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :get_hydrated)
    end
  end

  # Server functions

  @impl true
  def init(state) do
    Logger.notice("Starting the Atem server")
    Process.flag(:trap_exit, true)
    Phoenix.PubSub.subscribe(UpTally.PubSub, "atem_state")
    state = start(state)
    {:ok, state}
  end

  @impl true
  def terminate(_reason, state) do
    Logger.notice("Shutting down the Atem server")

    if state.socket != nil do
      :gen_udp.close(state.socket)
    end

    broadcast_disconnected()

    :normal
  end

  defp start(state) do
    broadcast_connecting()

    if state.status != :disconnected do
      Logger.debug("Closing existing socket")
      :gen_udp.close(state.socket)
    end

    random_session = Enum.random(20000..29999)

    state = %{
      state
      | status: :disconnected,
        retry_count: state.retry_count + 1,
        initial_session_id: random_session,
        session_id: random_session,
        remote_packet_id: 0,
        local_packet_id: 0,
        last_packet_time: 0,
        handshake_packet_count: 0
    }

    # state = set_ip_port(state)

    case :gen_udp.open(0, mode: :binary) do
      {:ok, socket} ->
        # broadcast_connected()
        Logger.debug("Socket opened")

        %{state | socket: socket, status: :hello}
        |> reset_alive_timer()
        |> send_hello()

      {:error, message} ->
        broadcast_disconnected()
        # TODO - Set a timer to restart the connection
        Logger.error("Failed to open socket.")
        IO.inspect(message)
        Process.send_after(self(), :start, get_retry_delay(state))
        %{state | socket: nil, status: :disconnected}
    end
  end

  defp send_hello(%{session_id: session_id} = state) do
    Logger.debug("Sending Hello")

    session_bytes = <<session_id::size(16)>>

    <<head_bytes::binary-size(2), _::binary-size(2), rest::binary>> = @hello

    message = head_bytes <> session_bytes <> rest

    send_message(state, message)
    %{state | status: :hello}
  end

  defp send_hello_ack(%{session_id: session_id} = state) do
    Logger.debug("Sending Hello Ack")
    session_bytes = <<session_id::size(16)>>

    <<head_bytes::binary-size(2), _::binary-size(2), rest::binary>> = @hello_ack
    # <<head_bytes::binary-size(2), _::binary-size(2), rest::binary>> = @ack
    message = head_bytes <> session_bytes <> rest

    send_message(
      state,
      message
    )

    %{state | status: :hello_ack}
  end

  defp maybe_send_ack(state, packet) do
    # <<bit1::bits-size(1), _rest::bits>> = packet

    if UpTally.Atem.AtemPacket.need_ack(packet) == true do
      session_bytes = <<state.session_id::size(16)>>
      packet_bytes = <<UpTally.Atem.AtemPacket.get_seq(packet)::size(16)>>

      <<head_bytes::binary-size(2), _::binary-size(2), _::binary-size(2), rest::binary>> = @ack

      message = head_bytes <> session_bytes <> packet_bytes <> rest
      # message = @ack |> :erlang.put_u16(2, session_id) |> :erlang.put_u16(4, packet_id)

      send_message(
        state,
        message
      )
    end

    state
  end

  defp maybe_request_retransmission(message, %{
         remote_packet_id: remote_packet_id,
         session_id: session_id
       }) do
    packet_seq = UpTally.Atem.AtemPacket.get_seq(message)
    packet_session_id = UpTally.Atem.AtemPacket.get_session(message)

    if packet_seq != remote_packet_id + 1 and session_id == packet_session_id do
      # Logger.debug("Requesting retransmission of #{Integer.to_string(packet_seq)}")
    end
  end

  # defp set_ip_port(state) do
  #   %{ip: ip, port: port} = Services.get_atem_settings()
  #   state |> Map.put(:ip, ip) |> Map.put(:port, port)
  # end

  # @impl true
  # def handle_call(:update_config, _sender, old_state) do
  #   updated_state = set_ip_port(old_state)

  #   if old_state.ip != updated_state.ip || old_state.port != updated_state.port do
  #     start(%{updated_state | retry_count: 0})
  #   else
  #     {:reply, :ok, old_state}
  #   end
  # end

  @impl true
  def handle_call(:get_state, _, state) do
    {:reply, {:ok, state}, state}
  end

  @impl true
  def handle_call(:get_hydrated, _caller, state) do
    {:reply, check_hydrated(state), state}
  end

  @impl true
  def handle_info(:start, state) do
    {:noreply, start(state)}
  end

  @impl true
  def handle_info(
        {:udp, _socket, source_ip, source_port, _message},
        %{ip: atem_ip} = state
      )
      when source_ip != atem_ip or source_port != 9910 do
    # Not for us, ignore it
    {:noreply, state}
  end

  @impl true
  def handle_info(
        {:udp, _socket, _ip, _port, message},
        %{status: status, session_id: session_id} = state
      )
      when status == :connected do
    broadcast_recieving()
    state = set_last_packet_time(state)
    maybe_request_retransmission(message, state)
    # message_list = :binary.bin_to_list(message)

    # Logger.debug(
    #   "Recieved: message from #{IP.to_string(ip)}:#{port} length: #{length(message_list)}"
    # )

    # If we're connected need to check the session_id matches ours
    case UpTally.Atem.AtemPacket.get_session(message) do
      packet_session_id when packet_session_id == session_id ->
        # Matched session, process data
        process_data(message, state)

      packet_session_id when packet_session_id == state.initial_session_id ->
        # Initial default session, somethings gone bad, let's restart!
        Process.send_after(self(), :start, 250)
        {:noreply, %{state | status: :disconnected, retry_count: state.retry_count + 1}}

      _ ->
        # Not for us, ignore it
        {:noreply, state}
    end
  end

  @impl true
  def handle_info({:udp, _socket, _ip, _port, message}, state) do
    broadcast_recieving()
    state = set_last_packet_time(state)
    # message_list = :binary.bin_to_list(message)

    # Logger.debug(
    #   "Recieved: message from #{IP.to_string(ip)}:#{port} length: #{length(message_list)}"
    # )

    process_data(message, state)
  end

  @impl true
  def handle_info(:check_alive, state) do
    now = DateTime.utc_now() |> DateTime.to_unix()

    if state.last_packet_time + @timeout < now do
      # Seems connection is dead, we should reconnect
      if state.alive_timer != nil do
        Process.cancel_timer(state.alive_timer)
      end

      broadcast_disconnected()
      Process.send_after(self(), :start, get_retry_delay(state))
      {:noreply, %{state | alive_timer: nil, last_packet_time: 0}}
    else
      {:noreply, state |> reset_alive_timer()}
    end
  end

  # imported methods

  @impl true
  def handle_info({:inputs, new_inputs}, %{inputs: old_inputs} = state) do
    state = set_hydrated(state, :inputs)
    updates = Enum.filter(new_inputs, &do_update?(old_inputs, &1))

    if length(updates) > 0 do
      updated_inputs =
        Enum.reduce(updates, old_inputs, fn updated_input, inputs ->
          Map.put(inputs, updated_input.video_src, updated_input)
        end)

      Phoenix.PubSub.broadcast(UpTally.PubSub, "atem", {:names, updated_inputs})
      {:noreply, state |> Map.put(:inputs, updated_inputs)}
    else
      {:noreply, state}
    end
  end

  @impl true
  def handle_info({:pgm_input, %{me: me, video_source: video_source}}, old_state) do
    state = old_state |> set_hydrated(:pgm_input) |> put_in([me, :program], video_source)
    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:pv_input, %{me: me, video_source: video_source}}, old_state) do
    state = old_state |> set_hydrated(:pv_input) |> put_in([me, :preview], video_source)
    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:transition, %{me: me, in_transition: in_transition}}, old_state) do
    state =
      old_state
      |> set_hydrated(:transition)
      |> put_in([me, :in_transition], in_transition)

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info(
        {:transition_settings,
         {me,
          %{
            style: style,
            style_next: style_next,
            transition: transition,
            transition_next: transition_next
          }}},
        old_state
      ) do
    state =
      old_state
      |> set_hydrated(:transition_settings)
      |> put_in(
        [me, :transition_settings],
        get_in(
          old_state,
          [me, :transition_settings]
        )
        |> Map.put(:style, style)
        |> Map.put(:style_next, style_next)
        |> Map.put(:transition, transition)
        |> Map.put(:transition_next, transition_next)
      )

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:upstream_key, {me, key, settings}}, old_state) do
    state =
      old_state
      |> set_hydrated(:upstream_key)
      |> Map.put(me, Map.put(old_state[me], key, settings))

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:upstream_key_on_air, {me, key, settings}}, old_state) do
    state =
      old_state
      |> set_hydrated(:upstream_key_on_air)
      |> Map.put(me, put_in(old_state[me], [key, :on_air], settings.on_air))

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:downstream_key, {dsk, settings}}, old_state) do
    state =
      old_state
      |> set_hydrated(:downstream_key)
      |> Map.put(dsk, update_downstream_key(old_state[dsk], settings))

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:downstream_key_on_air, {dsk, settings}}, old_state) do
    state =
      old_state
      |> set_hydrated(:downstream_key_on_air)
      |> Map.put(dsk, update_downstream_key_on_air(old_state[dsk], settings))

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:downstream_key_properties, {dsk, %{tie: tie}}}, old_state) do
    state =
      old_state
      |> set_hydrated(:downstream_key_properties)
      |> Map.put(dsk, Map.put(old_state[dsk], :tie, tie))

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info({:transition_dip, {me, %{dip_src: dip_src}}}, old_state) do
    state =
      old_state
      |> set_hydrated(:transition_dip)
      |> put_in([me, :transition_settings, :dip_src], dip_src)

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info(
        {:transition_wipe,
         {me, %{wipe_border_width: wipe_border_width, wipe_border_src: wipe_border_src}}},
        old_state
      ) do
    state =
      old_state
      |> set_hydrated(:transition_wipe)
      |> put_in([me, :transition_settings, :wipe_border_width], wipe_border_width)
      |> put_in([me, :transition_settings, :wipe_border_src], wipe_border_src)

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info(
        {:transition_dve, {me, _}},
        old_state
      )
      when me == :unknown do
    {:noreply, old_state}
  end

  @impl true
  def handle_info(
        {:transition_dve,
         {me,
          %{
            dve_fill_src: dve_fill_src,
            dve_key_src: dve_key_src,
            dve_key_enabled: dve_key_enabled,
            dve_effect_type: dve_effect_type
          }}},
        old_state
      ) do
    state =
      old_state
      |> set_hydrated(:transition_dve)
      |> put_in([me, :transition_settings, :dve_fill_src], dve_fill_src)
      |> put_in([me, :transition_settings, :dve_key_src], dve_key_src)
      |> put_in([me, :transition_settings, :dve_key_enabled], dve_key_enabled)
      |> put_in([me, :transition_settings, :dve_effect_type], dve_effect_type)

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info(
        {:supersource_box,
         {ss, box,
          %{
            enabled: enabled,
            source: source
          }}},
        old_state
      ) do
    state =
      old_state
      |> set_hydrated(:supersource_box)
      |> put_in([ss, box, :enabled], enabled)
      |> put_in([ss, box, :source], source)

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info(
        {:supersource_art,
         {box,
          %{
            fill_source: fill_source,
            key_source: key_source,
            foreground_background: foreground_background
          }}},
        old_state
      ) do
    state =
      old_state
      |> set_hydrated(:supersource_art)
      |> put_in([box, :fill_source], fill_source)
      |> put_in([box, :key_source], key_source)
      |> put_in([box, :foreground_background], foreground_background)

    maybe_send_update(old_state, state)
    {:noreply, state}
  end

  @impl true
  def handle_info(:connected, state) do
    {:noreply, Map.put(state, :connected, true)}
  end

  @impl true
  def handle_info(:disconnected, state) do
    {:noreply, Map.put(state, :connected, true)}
  end

  # end of imported methods

  defp process_data(message, state) do
    session_id = UpTally.Atem.AtemPacket.get_session(message)

    {:noreply,
     state
     |> maybe_set_session_id(session_id)
     |> set_packet_id(message)
     |> check_maximum_handshake_packets(session_id)
     |> message_recieved(message)}
  end

  defp maybe_set_session_id(
         %{session_id: session_id, initial_session_id: initial_session_id} = state,
         session
       )
       when session_id == initial_session_id and session !== initial_session_id do
    Logger.debug("Setting session ID to #{session}")
    %{state | session_id: session}
  end

  defp maybe_set_session_id(state, _session) do
    state
  end

  defp set_packet_id(
         %{remote_packet_id: remote_packet_id} = state,
         message
       ) do
    current_packet = UpTally.Atem.AtemPacket.get_seq(message)

    if current_packet > remote_packet_id do
      %{state | remote_packet_id: current_packet}
    else
      state
    end
  end

  defp check_maximum_handshake_packets(%{session_id: session_id} = state, remote_session_id)
       when session_id != remote_session_id,
       do: state

  defp check_maximum_handshake_packets(
         %{handshake_packet_count: handshake_packet_count, status: status} = state,
         _remote_session_id
       )
       when status != :connected and status !== :disconnected do
    if handshake_packet_count > @maximum_handshake_packets do
      Logger.error("Maximum handshake packets reached, reconnecting")
      Process.send_after(self(), :start, get_retry_delay(state))
      state
    else
      %{state | handshake_packet_count: handshake_packet_count + 1}
    end
  end

  defp check_maximum_handshake_packets(
         state,
         _remote_session_id
       ),
       do: state

  defp message_recieved(%{status: status} = state, message)
       when status == :hello and byte_size(message) == 20 do
    send_hello_ack(state)
  end

  defp message_recieved(%{status: status} = state, message)
       when status == :hello_ack and byte_size(message) == 12 do
    Logger.debug("Initial switcher state dump complete")

    if check_hydrated(state) == true do
      Logger.debug("Connected")
      broadcast_connected()
      maybe_send_ack(%{state | status: :connected, handshake_packet_count: 0}, message)
    else
      Logger.error("Failed to get full state, reconnecting")
      # broadcast_disconnected()
      # Process.send_after(self(), :start, get_retry_delay(state))
      # Process.send_after(self(), :start, get_retry_delay(state))
      broadcast_connected()
      Process.send_after(self(), :start, get_retry_delay(state))
      maybe_send_ack(%{state | status: :connected, retry_count: state.retry_count + 1}, message)
      # maybe_send_ack(%{state | status: :connected}, message)
      # %{state | retry_count: state.retry_count + 1}
      # {:noreply, %{state | alive_timer: nil, last_packet_time: 0, status: :disconnected}}
    end

    # broadcast_connected()
    # maybe_send_ack(%{state | status: :connected}, message)
  end

  defp message_recieved(%{status: status} = state, message)
       when status == :hello_ack do
    Logger.debug("Initial switcher state dump")
    AtemParser.parse_packet(message)
    maybe_send_ack(state, message)
  end

  # Ping response packet
  defp message_recieved(%{status: status} = state, message)
       when status == :connected and byte_size(message) == 12 do
    maybe_send_ack(state, message)
  end

  # Some kind of data!
  defp message_recieved(%{status: status} = state, message)
       when status == :connected do
    AtemParser.parse_packet(message)
    maybe_send_ack(state, message)
  end

  # Revieved some kind of data whilst disconnected, ignore it
  defp message_recieved(state, _message) do
    state
  end

  defp reset_alive_timer(state) do
    if state.alive_timer != nil do
      Process.cancel_timer(state.alive_timer)
    end

    %{
      state
      | alive_timer: Process.send_after(self(), :check_alive, 1000)
    }
  end

  defp set_last_packet_time(state) do
    %{
      state
      | last_packet_time: DateTime.utc_now() |> DateTime.to_unix(),
        retry_count: 0
    }
  end

  defp get_retry_delay(%{retry_count: retry_count}) when retry_count >= 10 do
    5000
  end

  defp get_retry_delay(%{retry_count: retry_count}) when retry_count == 0 do
    250
  end

  defp get_retry_delay(%{retry_count: retry_count}) do
    500 * retry_count
  end

  defp update_downstream_key(key, %{fill_source: fill_source, key_source: key_source}) do
    key |> Map.put(:fill_source, fill_source) |> Map.put(:key_source, key_source)
  end

  defp update_downstream_key_on_air(key, %{
         on_air: on_air,
         is_transitioning: is_transitioning,
         is_auto_transitioning: is_auto_transitioning
       }) do
    key
    |> Map.put(:on_air, on_air)
    |> Map.put(:is_transitioning, is_transitioning)
    |> Map.put(:is_auto_transitioning, is_auto_transitioning)
  end

  defp maybe_send_update(
         old_state,
         %{
           me1: me1,
           me2: me2,
           me3: me3,
           me4: me4,
           dsk1: dsk1,
           dsk2: dsk2,
           dsk3: dsk3,
           dsk4: dsk4,
           ss1: ss1,
           ss2: ss2
         } = state
       )
       when old_state != state do
    tally_state = %{
      me1_pv:
        me_pv(me1)
        |> transition_pv(me1)
        |> usk_pv(me1)
        |> dsk_pv(dsk1)
        |> dsk_pv(dsk2)
        |> dsk_pv(dsk3)
        |> dsk_pv(dsk4)
        |> ss(ss1)
        |> ss(ss2)
        |> Enum.uniq(),
      me1_pgm:
        me_pgm(me1)
        |> transition_pgm(me1)
        |> usk_pgm(me1)
        |> dsk_pgm(dsk1)
        |> dsk_pgm(dsk2)
        |> dsk_pgm(dsk3)
        |> dsk_pgm(dsk4)
        |> ss(ss1)
        |> ss(ss2)
        |> Enum.uniq(),
      me2_pv:
        me_pv(me2) |> transition_pv(me2) |> usk_pv(me2) |> ss(ss1) |> ss(ss2) |> Enum.uniq(),
      me2_pgm:
        me_pgm(me2) |> transition_pgm(me2) |> usk_pgm(me2) |> ss(ss1) |> ss(ss2) |> Enum.uniq(),
      me3_pv:
        me_pv(me3) |> transition_pv(me3) |> usk_pv(me3) |> ss(ss1) |> ss(ss2) |> Enum.uniq(),
      me3_pgm:
        me_pgm(me3) |> transition_pgm(me3) |> usk_pgm(me3) |> ss(ss1) |> ss(ss2) |> Enum.uniq(),
      me4_pv:
        me_pv(me4) |> transition_pv(me4) |> usk_pv(me4) |> ss(ss1) |> ss(ss2) |> Enum.uniq(),
      me4_pgm:
        me_pgm(me4) |> transition_pgm(me4) |> usk_pgm(me4) |> ss(ss1) |> ss(ss2) |> Enum.uniq()
    }

    Phoenix.PubSub.broadcast(UpTally.PubSub, "atem", {:updated_ui, state})

    Phoenix.PubSub.broadcast(UpTally.PubSub, "atem", {:routing, tally_state})
  end

  defp maybe_send_update(_old_state, _state) do
  end

  defp me_pv(me, active \\ [])

  defp me_pv(
         %{
           in_transition: in_transition,
           preview: preview,
           transition_settings: transition_settings
         },
         active
       )
       when in_transition == true do
    if transition_settings.transition.bg == true do
      active
    else
      active ++ [preview]
    end
  end

  defp me_pv(%{preview: preview}, active) do
    active ++ [preview]
  end

  defp usk_pv(active, %{in_transition: in_transition}) when in_transition == true do
    active
  end

  defp usk_pv(active, me) do
    active
    |> check_usk_pv(:key_1, me)
    |> check_usk_pv(:key_2, me)
    |> check_usk_pv(:key_3, me)
    |> check_usk_pv(:key_4, me)
  end

  defp check_usk_pv(active, key_name, me) do
    if me.transition_settings.transition[key_name] == true and me[key_name].on_air == false do
      check_usk_style(me[key_name], active)
    else
      active
    end
  end

  defp check_usk_style(
         %{key_type: key_type, fill_source: fill_source, key_source: key_source},
         active
       )
       when key_type == :luma do
    active ++ [fill_source, key_source]
  end

  defp check_usk_style(
         %{fill_source: fill_source},
         active
       ) do
    active ++ [fill_source]
  end

  defp dsk_pv(active, key) when key.on_air == false and key.tie == true,
    do: active ++ [key.fill_source, key.key_source]

  defp dsk_pv(active, _key),
    do: active

  defp me_pgm(me, active \\ [])

  defp me_pgm(
         %{
           in_transition: in_transition,
           program: program,
           preview: preview,
           transition_settings: transition_settings
         },
         active
       )
       when in_transition == true do
    if transition_settings.transition.bg == true do
      active ++ [program, preview]
    else
      active ++ [program]
    end
  end

  defp me_pgm(
         %{
           program: program
         },
         active
       ),
       do: active ++ [program]

  defp usk_pgm(active, me) do
    active
    |> check_usk_pgm(:key_1, me)
    |> check_usk_pgm(:key_2, me)
    |> check_usk_pgm(:key_3, me)
    |> check_usk_pgm(:key_4, me)
  end

  defp check_usk_pgm(active, key_name, me) do
    if me[key_name].on_air == true ||
         (me.in_transition == true and me.transition_settings.transition[key_name] == true) do
      check_usk_style(me[key_name], active)
    else
      active
    end
  end

  defp transition_pv(active, %{in_transition: in_transition}) when in_transition == true,
    do: active

  defp transition_pv(active, %{transition_settings: transition_settings}),
    do: get_transition_inputs(active, transition_settings)

  defp transition_pgm(active, %{
         in_transition: in_transition,
         transition_settings: transition_settings
       })
       when in_transition == true,
       do: get_transition_inputs(active, transition_settings)

  defp transition_pgm(active, _transition), do: active

  defp get_transition_inputs(active, %{style: style, dip_src: dip_src}) when style == :dip do
    active ++ [dip_src]
  end

  defp get_transition_inputs(active, %{style: style, dve_effect_type: dve_effect_type})
       when style == :dve and dve_effect_type == :none do
    active
  end

  defp get_transition_inputs(active, %{
         style: style,
         dve_fill_src: dve_fill_src,
         dve_key_enabled: dve_key_enabled,
         dve_key_src: dve_key_src
       })
       when style == :dve do
    if dve_key_enabled == true do
      active ++ [dve_fill_src, dve_key_src]
    else
      active ++ [dve_fill_src, dve_key_src]
    end
  end

  defp get_transition_inputs(active, %{
         style: style,
         wipe_border_width: wipe_border_width,
         wipe_border_src: wipe_border_src
       })
       when style == :wipe and wipe_border_width > 0 do
    active ++ [wipe_border_src]
  end

  defp get_transition_inputs(active, %{style: style}) when style == :wipe do
    active
  end

  defp get_transition_inputs(active, _transition_settings), do: active

  defp dsk_pgm(active, key) when key.on_air == true,
    do: active ++ [key.fill_source, key.key_source]

  defp dsk_pgm(active, _key),
    do: active

  defp ss(
         active,
         %{
           box1: box1,
           box2: box2,
           box3: box3,
           box4: box4
         } = ss
       ) do
    case Enum.find_index(active, &(&1 == 6000)) do
      nil ->
        active

      _ ->
        active
        |> map_ss_box(box1)
        |> map_ss_box(box2)
        |> map_ss_box(box3)
        |> map_ss_box(box4)
        |> map_ss_art(ss)
    end
  end

  defp map_ss_art(active, %{
         fill_source: fill_source,
         key_source: key_source,
         foreground_background: foreground_background
       })
       when foreground_background == :foreground do
    active ++ [fill_source, key_source]
  end

  defp map_ss_art(active, %{
         fill_source: fill_source
       }) do
    active ++ [fill_source]
  end

  defp map_ss_box(active, %{enabled: enabled, source: source}) when enabled == true do
    active ++ [source]
  end

  defp map_ss_box(active, _box), do: active

  defp do_update?(inputs, %UpTally.Atem.Parser.Input.Input{video_src: video_src} = new_input) do
    case Map.get(inputs, video_src) do
      nil ->
        true

      old_input ->
        has_changed?(old_input, new_input)
    end
  end

  defp has_changed?(old_input, new_input) do
    !Map.equal?(Map.from_struct(old_input), Map.from_struct(new_input))
  end

  defp set_hydrated(state, key) do
    if get_in(state, [:hydrated, key]) == false do
      Logger.debug("#{Atom.to_string(key)} Hydrated")
      state |> put_in([:hydrated, key], true)
    else
      state
    end
  end

  defp check_hydrated(state) do
    Map.to_list(state.hydrated)
    |> Enum.find_index(fn {_k, v} -> v == false end) == nil
  end

  defp via_tuple(id),
    do: {:via, Registry, {@registry, id}}

  defp broadcast_connecting() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :connecting}
    )
  end

  defp broadcast_recieving() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :recieving}
    )
  end

  defp broadcast_transmitting() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :transmitting}
    )
  end

  defp broadcast_connected() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem",
      :connected
    )

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :connected}
    )
  end

  defp broadcast_disconnected() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem",
      :disconnected
    )

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :disconnected}
    )
  end

  defp send_message(%{ip: ip, port: port, socket: socket}, message) do
    case :gen_udp.send(socket, ip, port, message) do
      :ok ->
        broadcast_transmitting()
        nil

      {:error, :enetunreach} ->
        Logger.error("UDP Send Failed, :enetunreach")

      {:error, reason} ->
        Logger.error("UDP Send Failed")
        IO.inspect(reason)
    end
  end
end
