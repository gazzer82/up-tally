defmodule UpTally.Services.AtemDummyServer do
  use GenServer
  @name __MODULE__

  @max_inputs 40

  require Logger
  alias UpTally.Atem.Parser.Input.Input

  @registry :atem_registry

  defmodule State do
    @typedoc """
        Type that represents the state for the atem service
    """

    use StructAccess
    @derive {Jason.Encoder, only: [:connected, :status]}
    @type t :: %__MODULE__{
            connected: boolean(),
            status: atom(),
            timer: reference(),
            current_index: integer(),
            inputs: list()
          }
    defstruct connected: false,
              status: :disconnected,
              timer: nil,
              current_index: 1,
              inputs: []
  end

  def start_link(atem_config) do
    name = via_tuple(atem_config.id)

    GenServer.start_link(@name, %State{}, name: name)
  end

  def reconnect(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :start)
    end
  end

  def update_config(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :update_config)
    end
  end

  def get_state(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :get_state)
    end
  end

  def is_hydrated?(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :get_hydrated)
    end
  end

  # Server functions

  @impl true
  def init(state) do
    Logger.notice("Starting the Atem DUMMY server")
    Process.flag(:trap_exit, true)
    Phoenix.PubSub.subscribe(UpTally.PubSub, "atem_state")
    state = start(state)
    {:ok, state}
  end

  @impl true
  def terminate(_reason, _state) do
    Logger.notice("Shutting down the DUMMY server")

    broadcast_disconnected()

    :normal
  end

  defp start(state) do
    broadcast_connecting()
    Process.send_after(self(), :tick, 5000)
    %{state | status: :disconnected}
  end

  @impl true
  def handle_call(:get_state, _, state) do
    {:reply, {:ok, state}, state}
  end

  @impl true
  def handle_call(:get_hydrated, _caller, state) do
    {:reply, true, state}
  end

  @impl true
  def handle_info(:start, state) do
    {:noreply, start(state)}
  end

  @impl true
  def handle_info(:tick, %{status: status} = state) when status == :disconnected do
    Process.send_after(self(), :tick, 2000)
    {:noreply, %{state | status: :connecting}}
  end

  @impl true
  def handle_info(:tick, %{status: status} = state) when status == :connecting do
    Process.send_after(self(), :tick, 2000)
    {:noreply, %{state | status: :hello}}
  end

  @impl true
  def handle_info(:tick, %{status: status} = state) when status == :hello do
    Process.send_after(self(), :tick, 2000)
    {:noreply, %{state | status: :hello_ack}}
  end

  @impl true
  def handle_info(:tick, %{status: status} = state) when status == :hello_ack do
    Process.send_after(self(), :tick, 2000)

    {:noreply, %{state | status: :connected}}
  end

  @impl true
  def handle_info(:tick, %{status: status} = state) when status == :connected do
    broadcast_transmitting()
    # Process.send_after(self(), :recieve, 1000)
    Process.send_after(self(), :tick, 2000)
    broadcast_connected()

    inputs =
      0..@max_inputs
      |> Enum.reduce(%{}, fn index, map ->
        Map.put(map, index, %Input{
          long_name: "Camera #{index}",
          short_name: "Camera #{index}",
          video_src: index
        })
      end)

    Phoenix.PubSub.broadcast(UpTally.PubSub, "atem", {:names, inputs})
    Phoenix.PubSub.broadcast(UpTally.PubSub, "atem", {:updated_ui, state})

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem",
      {:routing, set_pgm_pv(state.current_index, @max_inputs)}
    )

    {:noreply, %{state | inputs: inputs} |> set_index(@max_inputs)}
  end

  @impl true
  def handle_info(:recieve, state) do
    broadcast_recieving()
    {:noreply, state}
  end

  defp set_index(state, count) when state.current_index == count do
    %{state | current_index: 1}
  end

  defp set_index(state, _count) do
    %{state | current_index: state.current_index + 1}
  end

  defp set_pgm_pv(index, count) when index == count do
    %{
      me1_pgm: [1],
      me1_pv: [count],
      me2_pgm: [],
      me2_pv: [],
      me3_pgm: [],
      me3_pv: [],
      me4_pgm: [],
      me4_pv: []
    }
  end

  defp set_pgm_pv(index, _count) do
    %{
      me1_pgm: [index + 1],
      me1_pv: [index],
      me2_pgm: [],
      me2_pv: [],
      me3_pgm: [],
      me3_pv: [],
      me4_pgm: [],
      me4_pv: []
    }
  end

  defp via_tuple(id),
    do: {:via, Registry, {@registry, id}}

  defp broadcast_connecting() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :connecting}
    )
  end

  defp broadcast_recieving() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :recieving}
    )
  end

  defp broadcast_transmitting() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :transmitting}
    )
  end

  defp broadcast_connected() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem",
      :connected
    )

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :connected}
    )
  end

  defp broadcast_disconnected() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "atem",
      :disconnected
    )

    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "status",
      {:atem_status, :disconnected}
    )
  end
end
