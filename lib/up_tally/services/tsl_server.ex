defmodule UpTally.Services.TslServer do
  use GenServer
  @name __MODULE__

  @registry :tsl_registry

  # alias UpTally.Services

  require Logger

  defmodule State do
    @typedoc """
        Type that represents the state for the atem service
    """
    @type t :: %__MODULE__{
            connected: boolean(),
            socket: :socket,
            status: atom(),
            previous_state: map(),
            retry_count: integer(),
            tsl_send_timer: reference(),
            tsl_polling_timer: reference(),
            tsl_polling_index: integer(),
            msg_queue: [integer()],
            config: UpTally.Services.Tsl
          }
    defstruct connected: false,
              socket: nil,
              status: :disconnected,
              previous_state: %{},
              retry_count: 0,
              tsl_send_timer: nil,
              tsl_polling_timer: nil,
              tsl_polling_index: 0,
              msg_queue: [],
              config: nil
  end

  def start_link(tsl_config) do
    name = via_tuple(tsl_config.id)
    GenServer.start_link(@name, %State{config: tsl_config}, name: name)
  end

  def get_state(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :get_state)
    end

    # id |> via_tuple() |> GenServer.call(:get_state)
  end

  # Server functions

  @impl true
  def init(state) do
    Logger.notice("Starting the TSL server")
    Process.flag(:trap_exit, true)
    state = start(state)
    {:ok, state}
  end

  @impl true
  def terminate(_reason, state) do
    Logger.notice("Shutting down the TSL server")

    if state.socket != nil do
      :gen_tcp.close(state.socket)
    end

    :normal
  end

  defp start(state) do
    broadcast_connecting()
    Logger.debug("Socket opening")

    if state.status == :connected do
      Logger.debug("Closing existing socket")
      :gen_udp.close(state.socket)
    end

    if state.tsl_send_timer != nil do
      Logger.debug("Stopping TSL Send Timer")
      Process.cancel_timer(state.tsl_send_timer)
    end

    if state.tsl_polling_timer != nil do
      Logger.debug("Stopping TSL Polling Timer")
      Process.cancel_timer(state.tsl_polling_timer)
    end

    Logger.debug("Setting Initial State")

    state = %{state | previous_state: %{}, msg_queue: []}

    Logger.debug(
      "IP: #{state.config.ip}, Port: #{state.config.port}, IF: #{state.config.interface}"
    )

    udp_options = [
      :binary,
      reuseaddr: true,
      multicast_ttl: 128,
      ip: IP.from_string!(state.config.interface)
    ]

    case :gen_udp.open(0, udp_options) do
      {:ok, socket} ->
        # :ok = :inet.setopts(socket, add_membership: [state.ip, {0, 0, 0, 0}])
        Logger.debug("Socket opened")
        subscribe()
        broadcast_connected()
        %{state | socket: socket, status: :connected} |> start_timers() |> set_initial_state()

      {:error, message} ->
        # TODO - Set a timer to restart the connection
        Logger.error("Failed to open UDP socket: #{message}")
        broadcast_disconnected()
        Process.send_after(self(), :start, get_retry_delay(state))

        %{state | socket: nil, status: :disconnected, retry_count: state.retry_count + 1}
    end
  end

  defp get_retry_delay(%{retry_count: retry_count}) when retry_count >= 10 do
    5000
  end

  defp get_retry_delay(%{retry_count: retry_count}) when retry_count == 0 do
    250
  end

  defp get_retry_delay(%{retry_count: retry_count}) do
    500 * retry_count
  end

  defp set_initial_state(state) do
    Logger.debug("Getting Initial State")
    inputs = UpTally.Services.StateServer.get_inputs()
    send_data(state, inputs)
  end

  defp start_timers(state) do
    Logger.debug("Starting Timers")

    %{
      state
      | tsl_send_timer: Process.send_after(self(), :next_message, state.config.send_interval),
        tsl_polling_timer: Process.send_after(self(), :next_poll, state.config.polling_interval)
    }
  end

  defp subscribe do
    Phoenix.PubSub.subscribe(UpTally.PubSub, "state")
  end

  @impl true
  def handle_info(:start, state) do
    {:noreply, start(state)}
  end

  @impl true
  def handle_info({:routing, inputs}, state) do
    # IO.inspect(inputs)
    Logger.debug("Sending updated tally infomation over TSL")
    {:noreply, send_data(state, inputs)}
  end

  @impl true
  def handle_info(:timeout, state) do
    IO.puts("Socket timeout")
    {:noreply, start(state)}
  end

  @impl true
  def handle_info(:next_message, %{msg_queue: [head | tail]} = state) do
    if state.socket != nil and state.status == :connected and state.config.ip != nil and
         state.config.port != nil do
      :gen_udp.send(state.socket, IP.from_string!(state.config.ip), state.config.port, head)
      broadcast_transmitting()

      {:noreply,
       %{
         state
         | tsl_send_timer: Process.send_after(self(), :next_message, state.config.send_interval),
           msg_queue: tail
       }}
    else
      {:noreply,
       %{
         state
         | tsl_send_timer: Process.send_after(self(), :next_message, state.config.send_interval)
       }}
    end
  end

  @impl true
  def handle_info(:next_message, state) do
    {:noreply,
     %{
       state
       | tsl_send_timer: Process.send_after(self(), :next_message, state.config.send_interval)
     }}
  end

  @impl true
  def handle_info(
        :next_poll,
        state
      )
      when state.config.polling_enabled == true do
    case get_next_message(
           state.previous_state |> Enum.map(fn {_k, v} -> v end),
           state.tsl_polling_index,
           state.config.tsl_offset,
           state.config.tsl_version
         ) do
      {nil, index} ->
        {:noreply,
         %{
           state
           | tsl_polling_timer:
               Process.send_after(self(), :next_poll, state.config.polling_interval),
             tsl_polling_index: index
         }}

      {message, index} ->
        {:noreply,
         %{
           state
           | tsl_polling_timer:
               Process.send_after(self(), :next_poll, state.config.polling_interval),
             tsl_polling_index: index,
             msg_queue: state.msg_queue ++ [message]
         }}
    end
  end

  @impl true
  def handle_info(
        :next_poll,
        state
      ) do
    {:noreply,
     %{
       state
       | tsl_polling_timer: Process.send_after(self(), :next_poll, state.config.polling_interval)
     }}
  end

  @impl true
  def handle_info(_message, state) do
    {:noreply, state}
  end

  @impl true
  def handle_call(:get_state, _, state) do
    {:reply, {:ok, state}, state}
  end

  defp get_next_message(
         previous_state,
         tsl_polling_index,
         tsl_offset,
         tsl_protocol_version
       )
       when tsl_polling_index < length(previous_state) do
    {Enum.fetch!(previous_state, tsl_polling_index)
     |> create_tsl_message(tsl_offset, tsl_protocol_version), tsl_polling_index + 1}
  end

  defp get_next_message(
         previous_state,
         _tsl_polling_index,
         tsl_offset,
         tsl_protocol_version
       )
       when length(previous_state) > 0 do
    {Enum.fetch!(previous_state, 0)
     |> create_tsl_message(tsl_offset, tsl_protocol_version), 1}
  end

  defp get_next_message(
         _previous_state,
         _tsl_polling_index,
         _tsl_offset,
         _tsl_protocol_version
       ) do
    {nil, 1}
  end

  # @impl true
  # def handle_call(:update_config, _sender, old_state) do
  #   updated_state = old_state |> get_settings()

  #   if old_state.ip != updated_state.ip || old_state.port != updated_state.port ||
  #        old_state.tsl_offset != updated_state.tsl_offset ||
  #        old_state.if_ip != updated_state.if_ip ||
  #        old_state.tsl_send_interval != updated_state.tsl_send_interval ||
  #        old_state.tsl_polling_enabled != updated_state.tsl_polling_enabled ||
  #        old_state.tsl_polling_interval != updated_state.tsl_polling_interval do
  #     {:reply, :ok, start(%{updated_state | retry_count: 0})}
  #   else
  #     {:reply, :ok, old_state}
  #   end
  # end

  defp send_data(state, inputs) do
    inputs =
      set_monitored_mes(inputs, %{
        me1: state.config.me1,
        me2: state.config.me2,
        me3: state.config.me3,
        me4: state.config.me4
      })

    if state.socket != nil and state.status == :connected and state.config.ip != nil and
         state.config.port != nil do
      messages =
        filter_changed_set_priority(inputs, state.previous_state)
        |> create_messages(state.config.tsl_offset, state.config.tsl_version)

      messages = state.msg_queue ++ messages

      state
      |> Map.put(:previous_state, inputs)
      |> Map.put(
        :msg_queue,
        messages
      )

      # for input <- inputs |> filter_changed_set_priority(state.previous_state) do
      #   :gen_udp.send(state.socket, state.ip, state.port, create_message(input, state.tsl_offset))

      #   # :timer.sleep(state.tsl_send_interval)
      # end
    else
      Map.put(state, :previous_state, inputs)
    end
  end

  defp filter_changed_set_priority(inputs, previous_state) when inputs == previous_state do
    []
  end

  defp filter_changed_set_priority(inputs, previous_state) do
    inputs
    |> Enum.map(fn {_k, v} -> v end)
    |> Enum.sort_by(&set_priority/1, :desc)
    |> Enum.filter(fn input ->
      case Map.fetch(previous_state, input.input_number) do
        :error ->
          true

        {:ok, previous} ->
          previous.name != input.name || previous.on_pgm != input.on_pgm ||
            previous.on_pv != input.on_pv
      end
    end)
  end

  defp set_priority(%{on_pgm: on_pgm}) when on_pgm == true,
    do: 2

  defp set_priority(%{on_pv: on_pv}) when on_pv == true,
    do: 1

  defp set_priority(_current),
    do: 0

  defp create_messages(inputs, tsl_offset, tsl_protocol_version) do
    Enum.map(inputs, &create_tsl_message(&1, tsl_offset, tsl_protocol_version))
  end

  # defp create_tsl_message(%{input_number: input_number, name: name} = input, tsl_offset, protocol)
  #      when protocol == :tsl_5 do
  #   <<0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0xC1, 0x00, 0x0C, 0x00, 0x41, 0x42, 0x43,
  #     0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x50, 0x51, 0x52>>

  # List.duplicate(0x00, 2)
  # |> List.replace_at(0, 0x80 + (input_number - 1 + tsl_offset))
  # |> List.replace_at(1, get_tally_status(input))
  # |> List.replace_at(2, name)
  # |> add_name(name)
  # |> :binary.list_to_bin()
  # end

  defp create_tsl_message(input, tsl_offset, protocl, dle_stx \\ false)

  defp create_tsl_message(
         %{input_number: input_number, name: name} = input,
         tsl_offset,
         protocol,
         _dle_stx
       )
       when protocol == :tsl_v3 do
    List.duplicate(0x00, 2)
    |> List.replace_at(0, 0x80 + (input_number - 1 + tsl_offset))
    |> List.replace_at(1, get_tally_status_v3(input))
    # |> List.replace_at(2, name)
    |> add_name_v3(name)
    |> :binary.list_to_bin()
  end

  defp create_tsl_message(
         %{input_number: input_number, name: name} = input,
         tsl_offset,
         protocol,
         dle_stx
       )
       when protocol == :tsl_v5 do
    header = <<0xFE, 0x02>>
    version = <<0x00>>
    flags = <<0x00>>
    screen = <<0x00, 0x00>>
    name = get_name_v5(name)
    tally_status = get_tally_status_v5(input)

    message =
      version <>
        flags <>
        screen <>
        <<input_number - 1 + tsl_offset::little-size(16)>> <>
        tally_status <> <<String.length(name)::little-size(16)>> <> <<name::binary>>

    if dle_stx do
      header <> <<byte_size(message)::little-size(16)>> <> message
    else
      # <<0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0xC1, 0x00, 0x0C, 0x00, 0x43, 0x61, 0x6D,
      #   0x20, 0x32, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64>>
      <<byte_size(message)::little-size(16)>> <> message
    end
  end

  defp add_name_v3(message, name) do
    name_list = String.graphemes(name)

    if length(name_list) < 16 do
      padding = List.duplicate(0x20, 16 - length(name_list))
      message ++ name_list ++ padding
    else
      {keep, _discard} = Enum.split(name_list, 16)
      message ++ keep
    end
  end

  defp get_name_v5(name) do
    name_list = String.graphemes(name)

    if length(name_list) < 12 do
      padding = List.duplicate(0x64, 12 - length(name_list))
      (name_list ++ padding) |> List.to_string()
    else
      {keep, _discard} = Enum.split(name_list, 12)
      keep |> List.to_string()
    end
  end

  defp set_monitored_mes(channels, monitored_me) do
    # IO.inspect(channels)

    Enum.map(channels, &set_monitored_me(&1, monitored_me))
    |> Map.new(fn {ip, channel} -> {ip, channel} end)
  end

  defp set_monitored_me(
         {ip, %{input_number: input_number, name: name} = channel},
         monitored_me
       ) do
    {ip,
     %{
       on_pv:
         set_monitored_me(false, :me1, channel, :on_pv_me1, monitored_me)
         |> set_monitored_me(:me2, channel, :on_pv_me2, monitored_me)
         |> set_monitored_me(:me3, channel, :on_pv_me3, monitored_me)
         |> set_monitored_me(:me4, channel, :on_pv_me4, monitored_me),
       on_pgm:
         set_monitored_me(false, :me1, channel, :on_pgm_me1, monitored_me)
         |> set_monitored_me(:me2, channel, :on_pgm_me2, monitored_me)
         |> set_monitored_me(:me3, channel, :on_pgm_me3, monitored_me)
         |> set_monitored_me(:me4, channel, :on_pgm_me4, monitored_me),
       name: name,
       input_number: input_number
     }}
  end

  defp set_monitored_me(true, _me_key, _channel, _channel_key, _monitored_me), do: true

  defp set_monitored_me(state, me_key, channel, channel_key, monitored_me) do
    if Map.fetch!(monitored_me, me_key) == true do
      Map.fetch!(channel, channel_key)
    else
      state
    end
  end

  defp get_tally_status_v3(%{on_pv: on_pv, on_pgm: on_pgm})
       when on_pv == true and on_pgm == true do
    # for i <- [1, 1, 0, 0, 1, 1], do: <<i::1>>, into: <<>>
    35
  end

  defp get_tally_status_v3(%{on_pv: on_pv}) when on_pv == true do
    33
    # for i <- [1, 0, 0, 0, 1, 1], do: <<i::1>>, into: <<>>
  end

  defp get_tally_status_v3(%{on_pgm: on_pgm}) when on_pgm == true do
    50
  end

  defp get_tally_status_v3(_input) do
    48
    # for i <- [0, 0, 0, 0, 1, 1], do: <<i::1>>, into: <<>>
  end

  defp get_tally_status_v5(%{on_pv: on_pv, on_pgm: on_pgm})
       when on_pv == true and on_pgm == true do
    # for i <- [1, 1, 0, 0, 1, 1], do: <<i::1>>, into: <<>>
    <<0xC9, 0x00>>
  end

  defp get_tally_status_v5(%{on_pv: on_pv}) when on_pv == true do
    <<0xC8, 0x00>>
    # for i <- [1, 0, 0, 0, 1, 1], do: <<i::1>>, into: <<>>
  end

  defp get_tally_status_v5(%{on_pgm: on_pgm}) when on_pgm == true do
    <<0xC1, 0x00>>
  end

  defp get_tally_status_v5(_input) do
    <<0xC0, 0x00>>
    # for i <- [0, 0, 0, 0, 1, 1], do: <<i::1>>, into: <<>>
  end

  defp via_tuple(id),
    do: {:via, Registry, {@registry, id}}

  defp broadcast_connecting() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "tsl",
      :tsl_connecting
    )
  end

  defp broadcast_transmitting() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "tsl",
      :tsl_transmitting
    )
  end

  defp broadcast_connected() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "tsl",
      :tsl_connected
    )
  end

  defp broadcast_disconnected() do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "tsl",
      :tsl_disconnected
    )
  end
end
