defmodule UpTally.Services.VideohubController do
  use GenServer

  alias UpTally.Services.VideohubSupervisor

  @name __MODULE__

  # @status_check_interval 1000

  require Logger

  def start_link(_opts) do
    GenServer.start_link(
      @name,
      %{status: :unknown, status_timer: nil},
      name: @name
    )
  end

  def config_updated() do
    GenServer.cast(@name, :config_updated)
  end

  def get_status() do
    GenServer.call(@name, :get_status)
  end

  # Server functions

  @impl true
  def init(state) do
    Logger.notice("Starting the Videohub Controller")
    Process.flag(:trap_exit, true)

    # if state.status_timer != nil do
    #   Logger.debug("Stopping Status Timer")
    #   Process.cancel_timer(state.status_timer)
    # end

    Phoenix.PubSub.subscribe(UpTally.PubSub, "videohub")
    start()

    {:ok, state}
  end

  defp start() do
    # stop_all()
    start_all()
  end

  @impl true
  def terminate(_reason, _state) do
    Logger.notice("Shutting down the Videohub Controller")

    stop_all()

    :normal
  end

  defp stop_all() do
    VideohubSupervisor.stop_all()
  end

  defp start_all() do
    VideohubSupervisor.start_all()
  end

  @impl true
  def handle_cast(:config_updated, state) do
    start()
    {:noreply, state}
  end

  # @impl true
  # def handle_call(:get_status, _from, state) do
  #   {:reply, state.status, state}
  # end

  # def set_status(state) do
  #   case UpTally.Services.VideohubServer.get_state(1) do
  #     {:ok, status} ->
  #       %{state | status: status}

  #     %{status: :offline} ->
  #       %{state | status: :offline}
  #   end
  # end

  # @impl true
  # def handle_info(:update_status, state) do
  #   updated_state = state |> set_status()

  #   if updated_state.status != state.status do
  #     Phoenix.PubSub.broadcast(
  #       UpTally.PubSub,
  #       "status",
  #       {:videohub_state, updated_state.status}
  #     )
  #   end

  #   {:noreply,
  #    %{
  #      updated_state
  #      | status_timer: Process.send_after(self(), :update_status, @status_check_interval)
  #    }}
  # end

  @impl true
  def handle_info(status, state) do
    case status do
      :connecting ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:videohub_status, :connecting}
        )

      :tsl_connected ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:videohub_status, :connected}
        )

      :tsl_disconnected ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:videohub_status, :disconnected}
        )

      _ ->
        nil
    end

    {:noreply, state}
  end
end
