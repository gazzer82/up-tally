defmodule UpTally.Services.TslSupervisor do
  # Automatically defines child_spec/1
  @name __MODULE__

  alias UpTally.Services.TslServer

  alias UpTally.Services

  use DynamicSupervisor

  require Logger

  def start_all() do
    Services.list_tsl()
    |> Enum.each(fn tsl_config ->
      start_child(tsl_config)
    end)
  end

  def stop_all() do
    DynamicSupervisor.which_children(UpTally.Services.TslSupervisor)
    |> Enum.each(fn tsl_service ->
      case tsl_service do
        {:undefined, :restarting, _, _} ->
          nil

        {:undefined, pid, _, _} ->
          stop_child(pid)
      end
    end)
  end

  def start_link(init_arg) do
    DynamicSupervisor.start_link(@name, init_arg, name: @name)
  end

  def start_child(tsl_config) do
    child_specification = {TslServer, tsl_config}

    DynamicSupervisor.start_child(__MODULE__, child_specification)
  end

  def stop_child(pid) do
    DynamicSupervisor.terminate_child(__MODULE__, pid)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
