defmodule UpTally.Services.VideohubServer do
  use GenServer
  @name __MODULE__

  @registry :videohub_registry

  require Logger

  defmodule State do
    @typedoc """
        Type that represents the state for the videohub service
    """
    @type t :: %__MODULE__{
            id: integer(),
            ip: IP.addr(),
            port: integer(),
            input_count: integer(),
            output_count: integer(),
            socket: :socket,
            status: atom(),
            ping_timer: reference(),
            ping_response_timer: reference(),
            preamble: boolean(),
            retry_count: integer(),
            routing: map(),
            input_labels: map(),
            output_labels: map()
          }
    defstruct id: nil,
              ip: nil,
              port: nil,
              input_count: nil,
              output_count: nil,
              socket: nil,
              status: :disconnected,
              ping_timer: nil,
              ping_response_timer: nil,
              preamble: false,
              retry_count: 0,
              routing: %{},
              input_labels: %{},
              output_labels: %{}
  end

  def start_link(videohub_config) do
    name = via_tuple(videohub_config.id)

    GenServer.start_link(
      @name,
      %State{
        ip: IP.from_string!(videohub_config.ip),
        port: videohub_config.port,
        id: videohub_config.id
      },
      name: name
    )
  end

  def reconnect(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.call(pid, :start)
    end
  end

  def update_config(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        {:error, %{}}

      [{pid, _} | _tail] ->
        GenServer.cast(pid, :update_config)
    end
  end

  def get_state(id) do
    case Registry.lookup(@registry, id) do
      [] ->
        %{status: :offline}

      [{pid, _} | _tail] ->
        try do
          GenServer.call(pid, :get_state)
        catch
          _ -> %{status: :offline}
        end
    end
  end

  defp via_tuple(id),
    do: {:via, Registry, {@registry, id}}

  # Server functions

  @impl true
  def init(state) do
    Logger.notice("Starting the Videohub server")
    Process.flag(:trap_exit, true)
    state = start(state)
    {:ok, state}
  end

  @impl true
  def terminate(_reason, %{ip: ip, id: id} = state) do
    Logger.notice("Shutting down the Videohub server for #{IP.to_string(ip)}")

    if state.socket != nil do
      :gen_tcp.close(state.socket)
    end

    broadcast_disconnected(id)

    :normal
  end

  @impl true
  def handle_info(:start, state) do
    {:noreply, start(state)}
  end

  @impl true
  def handle_info({:tcp, _port, data}, state) do
    {:noreply, state |> process_data(data)}
  end

  @impl true
  def handle_info({:tcp_closed, _port}, state) do
    Logger.debug("Socket disconnected")
    broadcast_disconnected(state.id)
    {:noreply, start(%{state | status: :disconnected})}
  end

  @impl true
  def handle_info({:tcp_error, _port, :etimedout}, state) do
    Logger.debug("Socket timeout")
    broadcast_disconnected(state.id)
    {:noreply, start(%{state | status: :disconnected})}
  end

  @impl true
  def handle_info(:send_ping, state) do
    {:noreply, state |> send_ping() |> reset_timer()}
  end

  @impl true
  def handle_info(:ping_timeout, state) do
    broadcast_disconnected(state.id)
    {:noreply, start(%{state | status: :disconnected})}
  end

  @impl true
  def handle_info(:update_config, state) do
    {:reply, state |> send_message("video output routing:\n\n")}
  end

  @impl true
  def handle_call(:get_state, _, state) do
    {:reply, state, state}
  end

  defp start(state) do
    Logger.notice("Starting the Videohub server")

    Logger.debug("IP: #{IP.to_string(state.ip)}, Port: #{state.port}")

    if state.status == :connected do
      Logger.debug("Closing existing socket")
      :gen_tcp.close(state.socket)
      broadcast_disconnected(state.id)
    end

    opts = [inet_backend: :socket, send_timeout: 1000, send_timeout_close: true]

    Logger.debug("Socket connecting")

    case :gen_tcp.connect(state.ip, state.port, opts, 5000) do
      {:ok, socket} ->
        broadcast_connected(state.id)
        Logger.debug("Socket connected")
        %{state | socket: socket, status: :connected, retry_count: 0} |> reset_timer()

      {:error, message} ->
        broadcast_disconnected(state.id)
        Logger.error("Failed to connect to socket.")
        Logger.error("#{message}")
        broadcast_disconnected(state.id)
        Process.send_after(self(), :start, get_retry_delay(state))
        %{state | socket: nil, status: :disconnected, retry_count: state.retry_count + 1}
    end
  end

  defp get_retry_delay(%{retry_count: retry_count}) when retry_count >= 10 do
    5000
  end

  defp get_retry_delay(%{retry_count: retry_count}) when retry_count == 0 do
    250
  end

  defp get_retry_delay(%{retry_count: retry_count}) do
    500 * retry_count
  end

  defp send_ping(%{status: :connected} = state) do
    state = send_message(state, "PING:\n\n")
    %{state | ping_response_timer: Process.send_after(self(), :ping_timeout, 1000)}
  end

  defp send_ping(state) do
    %{state | ping_response_timer: Process.send_after(self(), :ping_timeout, 1000)}
  end

  defp send_message(state, message) do
    if state.status == :connected do
      case :gen_tcp.send(state.socket, message) do
        :ok ->
          state

        {:error, reason} ->
          Logger.error("Failed to send message: #{reason}")
          start(state)
      end
    end
  end

  defp reset_timer(state) do
    if state.ping_timer != nil do
      Process.cancel_timer(state.ping_timer)
    end

    %{state | ping_timer: Process.send_after(self(), :send_ping, 5 * 1000)}
  end

  defp reset_response_timer(state) do
    if state.ping_response_timer != nil do
      Process.cancel_timer(state.ping_response_timer)
    end

    state
  end

  defp process_data(state, data) do
    state =
      data
      |> to_string()
      |> String.split("\n\n", include_captures: true)
      |> Enum.map(&String.split(&1, "\n"))
      |> Enum.reduce(state, fn section, state ->
        state = update_state(state, section)
        state
      end)

    state
  end

  defp update_state(state, [head | _tail]) when head == "PROTOCOL PREAMBLE:" do
    Map.put(state, :preamble, true)
  end

  defp update_state(state, [head | _tail]) when head == "END PRELUDE:" do
    Logger.debug("End Preamble")
    Map.put(state, :preamble, false)
  end

  defp update_state(state, [head | tail]) when head == "VIDEOHUB DEVICE:" do
    Enum.map(tail, &String.split(&1, ":", trim: true))
    |> Enum.reduce(state, fn [key, val], acc ->
      case key do
        "Video inputs" ->
          Map.put(acc, :input_count, String.to_integer(String.trim(val)))

        "Video outputs" ->
          Map.put(acc, :output_count, String.to_integer(String.trim(val)))

        _ ->
          acc
      end
    end)
  end

  defp update_state(%{preamble: false} = state, [head | tail])
       when head == "INPUT LABELS:" do
    Logger.debug("Recieved - INPUT LABELS")
    broadcast_names(state.id, tail)
    update_input_labels(state, tail)
  end

  defp update_state(%{input_count: input_count} = state, [head | tail])
       when head == "INPUT LABELS:" and length(tail) == input_count do
    Logger.debug("Recieved - INPUT LABELS")
    broadcast_names(state.id, tail)
    update_input_labels(state, tail)
  end

  defp update_state(state, [head | tail]) when head == "INPUT LABELS:" do
    Logger.debug(
      "Partial input labels recieved, #{length(tail)} of #{state.input_count} fetching full state"
    )

    broadcast_names(state.id, tail)
    send_message(state, "INPUT LABELS:\n\n")
  end

  defp update_state(%{preamble: false} = state, [head | tail])
       when head == "OUTPUT LABELS:" do
    Logger.debug("Recieved - OUTPUT LABELS")
    broadcast_names(state.id, tail)
    update_output_labels(state, tail)
  end

  defp update_state(%{output_count: output_count} = state, [head | tail])
       when head == "OUTPUT LABELS:" and length(tail) == output_count do
    Logger.debug("Recieved - OUTPUT LABELS")
    broadcast_names(state.id, tail)
    update_output_labels(state, tail)
  end

  defp update_state(state, [head | tail]) when head == "OUTPUT LABELS:" do
    "Partial ouput labels recieved, #{length(tail)} of #{state.output_count} fetching full state"
    send_message(state, "OUTPUT LABELS:\n\n")
  end

  defp update_state(%{preamble: false} = state, [head | tail])
       when head == "VIDEO OUTPUT ROUTING:" do
    Logger.debug("Recieved - VIDEO OUTPUT ROUTING")
    IO.inspect(tail)
    broadcast_routing(state.id, tail)
    update_routing(state, tail)
  end

  defp update_state(%{output_count: output_count} = state, [head | tail])
       when head == "VIDEO OUTPUT ROUTING:" and length(tail) == output_count do
    Logger.debug("Recieved - VIDEO OUTPUT ROUTING")
    IO.inspect(tail)
    broadcast_routing(state.id, tail)
    update_routing(state, tail)
  end

  defp update_state(state, [head | tail]) when head == "VIDEO OUTPUT ROUTING:" do
    "Partial ouput routing recieved, #{length(tail)} of #{state.output_count} fetching full state"
    send_message(state, "VIDEO OUTPUT ROUTING:\n\n")
  end

  defp update_state(state, [head | _tail]) when head == "ACK" do
    reset_response_timer(state)
  end

  defp update_state(state, _data) do
    state
  end

  defp update_input_labels(state, labels) do
    Map.put(
      state,
      :input_labels,
      Enum.reduce(labels, state.input_labels, fn label, current ->
        [input | rest] = String.split(label, " ")
        Map.put(current, String.to_integer(input) + 1, Enum.join(rest, " "))
      end)
    )
  end

  defp update_output_labels(state, labels) do
    Map.put(
      state,
      :output_labels,
      Enum.reduce(labels, state.output_labels, fn label, current ->
        [output | rest] = String.split(label, " ")
        Map.put(current, String.to_integer(output) + 1, Enum.join(rest, " "))
      end)
    )
  end

  defp update_routing(state, routing) do
    Map.put(
      state,
      :routing,
      Enum.reduce(routing, state.routing, fn route, current ->
        [output | [input]] = String.split(route, " ")
        Map.put(current, String.to_integer(output) + 1, String.to_integer(input) + 1)
      end)
    )
  end

  defp broadcast_connected(id) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "videohub",
      {:connected, id}
    )
  end

  defp broadcast_disconnected(id) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "videohub",
      {:disconnected, id}
    )
  end

  defp broadcast_routing(id, data) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "videohub",
      {:routing, id, data}
    )
  end

  defp broadcast_names(id, data) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "videohub",
      {:names, id, data}
    )
  end
end
