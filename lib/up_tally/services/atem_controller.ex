defmodule UpTally.Services.AtemController do
  use GenServer

  alias UpTally.Services.AtemSupervisor

  @name __MODULE__

  @status_check_interval 1000

  require Logger

  def start_link(_opts) do
    GenServer.start_link(
      @name,
      %{status: :unknown, status_timer: nil},
      name: @name
    )
  end

  def config_updated() do
    GenServer.cast(@name, :config_updated)
  end

  def get_status() do
    GenServer.call(@name, :get_status)
  end

  # Server functions

  @impl true
  def init(state) do
    Logger.notice("Starting the Atem Controller")
    Process.flag(:trap_exit, true)

    if state.status_timer != nil do
      Logger.debug("Stopping Status Timer")
      Process.cancel_timer(state.status_timer)
    end

    Phoenix.PubSub.subscribe(UpTally.PubSub, "atem")
    start()

    {:ok,
     %{state | status_timer: Process.send_after(self(), :update_status, @status_check_interval)}
     |> set_status()}
  end

  defp start() do
    stop_all()
    start_all()
  end

  @impl true
  def terminate(_reason, _state) do
    Logger.notice("Shutting down the Atem Controller")

    stop_all()

    :normal
  end

  defp stop_all() do
    AtemSupervisor.stop_all()
  end

  defp start_all() do
    AtemSupervisor.start_all()
  end

  @impl true
  def handle_cast(:config_updated, state) do
    start()
    {:noreply, state}
  end

  @impl true
  def handle_call(:get_status, _from, state) do
    {:reply, state.status, state}
  end

  def set_status(state) do
    case UpTally.Services.AtemServer.get_state(1) do
      {:ok, status} ->
        %{state | status: status}

      {:error, _} ->
        %{state | status: :unknown}
    end
  end

  @impl true
  def handle_info(:update_status, state) do
    updated_state = state |> set_status()

    if updated_state.status != state.status do
      Phoenix.PubSub.broadcast(
        UpTally.PubSub,
        "status",
        {:atem_state, updated_state.status}
      )
    end

    {:noreply,
     %{
       updated_state
       | status_timer: Process.send_after(self(), :update_status, @status_check_interval)
     }}
  end

  @impl true
  def handle_info(status, state) do
    case status do
      :connecting ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:tsl_status, :connecting}
        )

      :tsl_connected ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:tsl_status, :connected}
        )

      :tsl_disconnected ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:tsl_status, :disconnected}
        )

      :tsl_transmitting ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:tsl_status, :transmitting}
        )

      :tsl_recieving ->
        Phoenix.PubSub.broadcast(
          UpTally.PubSub,
          "status",
          {:tsl_status, :recieving}
        )

      _ ->
        nil
    end

    {:noreply, state}
  end
end
