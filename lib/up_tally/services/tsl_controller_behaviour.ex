defmodule UpTally.Services.TslControllerBehaviour do
  @callback config_updated() :: nil
  @callback get_status() :: Struct
end
