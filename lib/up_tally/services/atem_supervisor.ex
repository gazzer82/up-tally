defmodule UpTally.Services.AtemSupervisor do
  # Automatically defines child_spec/1
  @name __MODULE__

  alias UpTally.Services.AtemServer
  alias UpTally.Services.AtemDummyServer

  alias UpTally.Services

  use DynamicSupervisor

  require Logger

  def start_all() do
    Logger.debug("Starting Atem Child")

    Services.list_atem()
    |> Enum.each(fn atem_config ->
      start_child(atem_config)
    end)

    # Services.list_tsl()
    # |> Enum.each(fn tsl_config ->
    #   start_child(tsl_config)
    # end)
  end

  def stop_all() do
    DynamicSupervisor.which_children(__MODULE__)
    |> Enum.each(fn atem_service ->
      case atem_service do
        {:undefined, :restarting, _, _} ->
          nil

        {:undefined, pid, _, _} ->
          stop_child(pid)
      end
    end)
  end

  def start_link(init_arg) do
    DynamicSupervisor.start_link(@name, init_arg, name: @name)
  end

  def start_child(%UpTally.Services.Atem{test: test} = atem_config) when test == true do
    child_specification = {AtemDummyServer, atem_config}

    DynamicSupervisor.start_child(__MODULE__, child_specification)
  end

  def start_child(atem_config) do
    child_specification = {AtemServer, atem_config}

    DynamicSupervisor.start_child(__MODULE__, child_specification)
  end

  def stop_child(pid) do
    DynamicSupervisor.terminate_child(__MODULE__, pid)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
