defmodule UpTally.Services.Tsl do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tsl" do
    field :interface, EctoFields.IP
    field :ip, EctoFields.IP
    field :name, :string
    field :port, :integer
    field :tsl_version, Ecto.Enum, values: [:tsl_v3, :tsl_v5]
    field :send_interval, :integer
    field :tsl_offset, :integer
    field :polling_interval, :integer
    field :polling_enabled, :boolean
    field :me1, :boolean
    field :me2, :boolean
    field :me3, :boolean
    field :me4, :boolean

    timestamps()
  end

  @doc false
  def changeset(tsl, attrs) do
    tsl
    |> cast(attrs, [
      :name,
      :ip,
      :port,
      :interface,
      :tsl_version,
      :send_interval,
      :tsl_offset,
      :polling_interval,
      :polling_enabled,
      :me1,
      :me2,
      :me3,
      :me4
    ])
    |> validate_required([
      :name,
      :ip,
      :port,
      :interface,
      :tsl_version,
      :send_interval,
      :tsl_offset,
      :polling_interval,
      :polling_enabled
    ])
    |> validate_if_ip(:interface)
  end

  defp validate_if_ip(changeset, field) when is_atom(field) do
    validate_change(changeset, field, fn field, value ->
      case UpTally.Interfaces.is_valid?(value) do
        true ->
          []

        false ->
          [{field, "Is not a valid local interface address."}]
      end
    end)
  end
end
