defmodule UpTally.Services.VideohubSupervisor do
  # Automatically defines child_spec/1
  @name __MODULE__

  alias UpTally.Services.VideohubServer

  alias UpTally.Services

  use DynamicSupervisor

  require Logger

  def start_all() do
    Logger.info("Starting Videohub Supervisor")

    Services.list_videohub()
    |> Enum.each(fn videohub_config ->
      start_child(videohub_config)
    end)
  end

  def stop_all() do
    DynamicSupervisor.which_children(__MODULE__)
    |> Enum.each(fn videohub_service ->
      case videohub_service do
        {:undefined, :restarting, _, _} ->
          nil

        {:undefined, pid, _, _} ->
          stop_child(pid)
      end
    end)
  end

  def start_link(init_arg) do
    DynamicSupervisor.start_link(@name, init_arg, name: @name)
  end

  def start_child(videohub_config) do
    child_specification = {VideohubServer, videohub_config}
    DynamicSupervisor.start_child(__MODULE__, child_specification)
  end

  def stop_child(pid) do
    DynamicSupervisor.terminate_child(__MODULE__, pid)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
