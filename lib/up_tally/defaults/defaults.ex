defmodule UpTally.Defaults do
  def atem_crosspoints do
    ~S(
      VIDEO OUTPUT ROUTING:
        0 41
        1 41
        2 41
        3 41
        4 41
        5 41
        6 38
        7 24
        8 26
        9 21
        10 41
        11 43
        12 1
        13 2
        14 21
        15 22
    )
  end

  def atem_input_labels do
    ~S(
    INPUT LABELS:
      0 Black
      1 Camera 1
      2 Camera 2
      3 Camera 3
      4 Camera 4
      5 Camera 5
      6 Camera 6
      7 Camera 7
      8 Camera 8
      9 Camera 9
      10 Camera 10
      11 Camera 11
      12 Camera 12
      13 Camera 13
      14 Camera 14
      15 Camera 15
      16 Camera 16
      17 Camera 17
      18 Camera 18
      19 Camera 19
      20 Camera 20
      21 Color Bars
      22 Color 1
      23 Color 2
      24 Media Player 1
      25 Media Player 1 Key
      26 Media Player 2
      27 Media Player 2 Key
      28 ME 1 Key 1 Mask
      29 ME 1 Key 2 Mask
      30 ME 1 Key 3 Mask
      31 ME 1 Key 4 Mask
      32 ME 2 Key 1 Mask
      33 ME 2 Key 2 Mask
      34 ME 2 Key 3 Mask
      35 ME 2 Key 4 Mask
      36 DSK 1 Mask
      37 DSK 2 Mask
      38 SuperSource 1
      39 Clean Feed 1
      40 Clean Feed 2
      41 ME 1
      42 ME 1 PVW
      43 ME 2
      44 ME 2 PVW
    )
  end

  def atem_ouput_labels do
    ~S(
      OUTPUT LABELS:
        0 Output 1
        1 Output 2
        2 Output 3
        3 Output 4
        4 Output 5
        5 Output 6
        6 Output 7
        7 Output 8
        8 Output 9
        9 Output 10
        10 Output 11
        11 Output 12
        12 ME 1
        13 ME 1 PVW
        14 ME 2
        15 ME 2 PVW
    )
  end

  def videohub_crosspoints do
    ~S(
      VIDEO OUTPUT ROUTING:
        0 0
        1 1
        2 2
        3 3
        4 4
        5 5
        6 6
        7 7
        8 8
        9 9
        10 10
        11 11
        12 12
        13 13
        14 14
        15 15
        16 16
        17 17
        18 18
        19 19
        20 20
        21 21
        22 22
        23 23
        24 24
        25 25
        26 26
        27 27
        28 28
        29 29
        30 30
        31 31
        32 30
        33 30
        34 30
        35 30
        36 34
        37 33
        38 38
        39 39
    )
  end

  def videohub_input_labels do
    ~S(
      INPUT LABELS:
        0 1 Cam1
        1 2 Cam2
        2 3 Cam3
        3 4 Cam4
        4 5 Cam5
        5 6 Cam6
        6 7 Cam7
        7 8 Cam8
        8 Input 9
        9 Input 10
        10 Input 11
        11 Input 12
        12 Input 13
        13 Input 14
        14 Input 15
        15 Input 16
        16 Input 17
        17 Input 18
        18 Input 19
        19 Input 20
        20 21 Aux1
        21 22 Aux2
        22 23 Aux3
        23 24 Aux4
        24 25 Aux5
        25 26 Aux6
        26 27 Aux7
        27 28 Aux8
        28 29 Aux9
        29 30 Aux10
        30 31 Aux11
        31 32 Aux12
        32 33 MV1
        33 34 MV2
        34 35 PC
        35 Input 36
        36 Input 37
        37 Input 38
        38 Input 39
        39 Input 40
    )
  end

  def videohub_ouput_labels do
    ~S(
      OUTPUT LABELS:
        0 1 Mix1
        1 2 Mix2
        2 3 Mix3
        3 4 Mix4
        4 5 Mix5
        5 6 Mix6
        6 7 Mix7
        7 8 Mix8
        8 9 Mix9
        9 10 Mix10
        10 11 Mix11
        11 12 Mix12
        12 13 Mix13
        13 14 Mix14
        14 15 Mix15
        15 16 Mix16
        16 17 Mix17
        17 18 Mix18
        18 19 Mix19
        19 20 Mix20
        20 Output 21
        21 Output 22
        22 Output 23
        23 Output 24
        24 Output 25
        25 Output 26
        26 Output 27
        27 Output 28
        28 Output 29
        29 Output 30
        30 Output 31
        31 Output 32
        32 33 Vis1
        33 34 Vis2
        34 35 CamRtn1
        35 36 CamRtn2
        36 37 DirMon1
        37 38 DirMon2
        38 Output 39
        39 Output 40
    )
  end
end
