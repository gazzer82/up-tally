defmodule UpTally.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    UpTally.Release.migrate()

    children = [
      UpTallyWeb.Telemetry,
      UpTally.Repo,
      {DNSCluster, query: Application.get_env(:sample_app, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: UpTally.PubSub},
      # Start the Registry
      {Registry, [keys: :unique, name: :tsl_registry]},
      {Registry, [keys: :unique, name: :atem_registry]},
      {Registry, [keys: :unique, name: :videohub_registry]},
      # Start the service supervisor
      UpTally.ServicesSupervisor,
      # {SampleApp.Worker, arg},
      # Start to serve requests, typically the last entry
      UpTallyWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: UpTally.Supervisor]
    result = Supervisor.start_link(children, opts)
    Logger.add_backend(UpTally.PubSubLogger)
    result
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    UpTallyWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
