defmodule UpTally.Services do
  require Logger

  import Ecto.Query, warn: false
  alias UpTally.Repo

  alias UpTally.Services.Tsl

  defp tsl_controller,
    do: Application.get_env(:up_tally, :tsl_controller, UpTally.Services.TslController)

  defp tsl_server,
    do: Application.get_env(:up_tally, :tsl_server, UpTally.Services.TslServer)

  @doc """
  Returns the list of tsl.

  ## Examples

      iex> list_tsl()
      [%Tsl{}, ...]

  """
  def list_tsl do
    Repo.all(Tsl)
  end

  @doc """
  Gets a single tsl.

  Raises `Ecto.NoResultsError` if the Tsl does not exist.

  ## Examples

      iex> get_tsl!(123)
      %Tsl

      iex> get_tsl!(456)
      ** (Ecto.NoResultsError)

  """
  def get_tsl!(id), do: Repo.get!(Tsl, id)

  @doc """
  Creates a tsl.

  ## Examples

      iex> create_tsl(%{field: value})
      {:ok, %Tsl{}}

      iex> create_tsl(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_tsl(attrs \\ %{}) do
    case Repo.insert(Tsl.changeset(%Tsl{}, attrs)) do
      {:ok, tsl} ->
        tsl_config_updated()
        {:ok, tsl}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Updates a tsl.

  ## Examples

      iex> update_tsl(tsl, %{field: new_value})
      {:ok, %Tsl{}}

      iex> update_tsl(tsl, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_tsl(%Tsl{} = tsl, attrs) do
    case Repo.update(Tsl.changeset(tsl, attrs)) do
      {:ok, tsl} ->
        tsl_config_updated()
        {:ok, tsl}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Deletes a tsl.

  ## Examples

      iex> delete_tsl(tsl)
      {:ok, %Tsl{}}

      iex> delete_tsl(tsl)
      {:error, %Ecto.Changeset{}}

  """
  def delete_tsl(%Tsl{} = tsl) do
    case Repo.delete(tsl) do
      {:ok, tsl} ->
        tsl_config_updated()
        {:ok, tsl}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking tsl changes, contains sensible defaults.

  ## Examples

      iex> change_tsl(tsl)
      %Ecto.Changeset{data: %Tsl{}}

  """
  def change_tsl(%Tsl{} = _tsl, attrs \\ %{}) do
    Tsl.changeset(
      %Tsl{
        ip: "239.1.2.3",
        interface: UpTally.Interfaces.get_default_interface_string(),
        port: 40001,
        tsl_version: :tsl_v3,
        send_interval: 50,
        polling_interval: 200,
        polling_enabled: true,
        tsl_offset: 0
      },
      attrs
    )
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking tsl changes when editing an existing record.

  ## Examples

      iex> change_update_tsl(tsl)
      %Ecto.Changeset{data: %Tsl{}}

  """
  def change_update_tsl(%Tsl{} = tsl, attrs \\ %{}) do
    Tsl.changeset(tsl, attrs)
  end

  @doc """
  Notifies the TSL controller that the config has been updated
  """
  def tsl_config_updated() do
    tsl_controller().config_updated()
  end

  @doc """
  Gets the status of all current TSl Clients

  ## Examples
      iex> get_status()
      %{unknown: 0, connecting: 0, connected: 0, disconnected: 0}
  """
  def get_tsl_status() do
    tsl_controller().get_status()
  end

  def get_tsl_state(id) do
    case tsl_server().get_state(id) do
      {:error, _} ->
        :unknown

      {:ok, result} ->
        result.status
    end
  end

  alias UpTally.Services.Atem

  defp atem_controller,
    do: Application.get_env(:up_tally, :atem_controller, UpTally.Services.AtemController)

  # defp atem_server,
  #   do: Application.get_env(:up_tally, :atem_server, UpTally.Services.AtemServer)

  @doc """
  Returns the list of atem.

  ## Examples

      iex> list_atem()
      [%Atem{}, ...]

  """
  def list_atem do
    Repo.all(Atem)
  end

  @doc """
  Gets a single atem.

  Raises `Ecto.NoResultsError` if the Atem does not exist.

  ## Examples

      iex> get_atem!(123)
      %Atem{}

      iex> get_atem!(456)
      ** (Ecto.NoResultsError)

  """
  def get_atem!(id), do: Repo.get!(Atem, id)

  @doc """
  Creates a atem.

  ## Examples

      iex> create_atem(%{field: value})
      {:ok, %Atem{}}

      iex> create_atem(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_atem(attrs \\ %{}) do
    case Repo.insert(Atem.changeset(%Atem{}, attrs)) do
      {:ok, atem} ->
        atem_config_updated()
        {:ok, atem}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Updates a atem.

  ## Examples

      iex> update_atem(atem, %{field: new_value})
      {:ok, %Atem{}}

      iex> update_atem(atem, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_atem(%Atem{} = atem, attrs) do
    case Repo.update(Atem.changeset(atem, attrs)) do
      {:ok, atem} ->
        atem_config_updated()
        {:ok, atem}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Deletes a atem.

  ## Examples

      iex> delete_atem(atem)
      {:ok, %Atem{}}

      iex> delete_atem(atem)
      {:error, %Ecto.Changeset{}}

  """
  def delete_atem(%Atem{} = atem) do
    case Repo.delete(atem) do
      {:ok, atem} ->
        atem_config_updated()
        {:ok, atem}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking atem changes. Contains a sane default IP and port

  ## Examples

      iex> change_atem(atem)
      %Ecto.Changeset{data: %Atem{}}

  """
  def change_atem(%Atem{} = _atem, attrs \\ %{}) do
    Atem.changeset(
      %Atem{
        ip: "192.168.10.240",
        port: 9910
      },
      attrs
    )
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking atem changes when editing an existing record

  ## Examples

      iex> change_update_atem(atem)
      %Ecto.Changeset{data: %Atem{}}

  """
  def change_update_atem(%Atem{} = atem, attrs \\ %{}) do
    Atem.changeset(
      atem,
      attrs
    )
  end

  @doc """
  Notifies the Atem controller that the config has been updated
  """
  def atem_config_updated() do
    atem_controller().config_updated()
  end

  @doc """
  Maps the DB Atem model to the pretty version
  """
  def get_pretty_atem_model(%Atem{model: model}) do
    case model do
      "constellation_2me" ->
        "Constellation 2ME"

      "constellation_4me" ->
        "Constellation 4ME"
    end
  end

  alias UpTally.Services.Videohub

  defp videohub_controller,
    do: Application.get_env(:up_tally, :videohub_controller, UpTally.Services.VideohubController)

  @doc """
  Returns the list of videohub.

  ## Examples

      iex> list_videohub()
      [%Videohub{}, ...]

  """
  def list_videohub do
    Repo.all(Videohub)
  end

  @doc """
  Returns the list of videohub with the current server status

  ## Examples

      iex> list_videohub_with_status()
      [%Videohub{status: %{}}, ...]

  """
  def list_videohub_with_status do
    Repo.all(Videohub)
    |> Enum.map(fn videohub ->
      Map.put(videohub, :status, UpTally.Services.VideohubServer.get_state(videohub.id))
    end)
  end

  @doc """
  Gets a single videohub.

  Raises `Ecto.NoResultsError` if the Videohub does not exist.

  ## Examples

      iex> get_videohub!(123)
      %Videohub{}

      iex> get_videohub!(456)
      ** (Ecto.NoResultsError)

  """
  def get_videohub!(id), do: Repo.get!(Videohub, id)

  @doc """
  Creates a videohub.

  ## Examples

      iex> create_videohub(%{field: value})
      {:ok, %Videohub{}}

      iex> create_videohub(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_videohub(attrs \\ %{}) do
    case Repo.insert(Videohub.changeset(%Videohub{}, attrs)) do
      {:ok, videohub} ->
        videohub_config_updated()
        {:ok, videohub}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Updates a videohub.

  ## Examples

      iex> update_videohub(videohub, %{field: new_value})
      {:ok, %Videohub{}}

      iex> update_videohub(videohub, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_videohub(%Videohub{} = videohub, attrs) do
    case Repo.update(Videohub.changeset(videohub, attrs)) do
      {:ok, videohub} ->
        videohub_config_updated()
        {:ok, videohub}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Deletes a videohub.

  ## Examples

      iex> delete_videohub(videohub)
      {:ok, %Videohub{}}

      iex> delete_videohub(videohub)
      {:error, %Ecto.Changeset{}}

  """
  def delete_videohub(%Videohub{} = videohub) do
    case Repo.delete(videohub) do
      {:ok, videohub} ->
        videohub_config_updated()
        {:ok, videohub}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking videohub changes. Comtains a sane default IP and port.

  ## Examples

      iex> change_videohub(videohub)
      %Ecto.Changeset{data: %Videohub{}}

  """
  def change_videohub(%Videohub{} = videohub, attrs \\ %{}) do
    Videohub.changeset(%Videohub{} = videohub, attrs)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking videohub changes when editing an existing record

  ## Examples

      iex> change_update_videohub(videohub)
      %Ecto.Changeset{data: %Videohub{}}

  """
  def change_update_videohub(%Videohub{} = videohub, attrs \\ %{}) do
    Videohub.changeset(
      videohub,
      attrs
    )
  end

  @doc """
  Notifies the Atem controller that the config has been updated
  """
  def videohub_config_updated() do
    videohub_controller().config_updated()
  end
end
