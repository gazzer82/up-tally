defmodule UpTally.Services.StateServer do
  use GenServer
  @name __MODULE__

  require Logger

  # @me1_pgm 12
  # @me1_pv 13
  # @me2_pgm 14
  # @me2_pv 15

  defmodule State do
    @typedoc """
        Type that represents the state for the atem service
    """
    @type t :: %__MODULE__{
            atem_status: atom(),
            tsl_status: atom(),
            inputs: map()
          }
    defstruct connected: false,
              atem_status: :disconnected,
              tsl_status: :disconnected,
              inputs: %{}
  end

  defmodule Input do
    @type t :: %__MODULE__{
            input_number: integer(),
            name: String.t(),
            on_pv_me1: boolean(),
            on_pgm_me1: boolean(),
            on_pv_me2: boolean(),
            on_pgm_me2: boolean(),
            on_pv_me3: boolean(),
            on_pgm_me3: boolean(),
            on_pv_me4: boolean(),
            on_pgm_me4: boolean()
          }
    defstruct input_number: nil,
              name: nil,
              on_pv_me1: false,
              on_pgm_me1: false,
              on_pv_me2: false,
              on_pgm_me2: false,
              on_pv_me3: false,
              on_pgm_me3: false,
              on_pv_me4: false,
              on_pgm_me4: false
  end

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, %State{}, name: @name)
  end

  def get_inputs() do
    GenServer.call(@name, :get_inputs)
  end

  def get_atem_status() do
    GenServer.call(@name, :get_atem_status)
  end

  def get_tsl_status() do
    GenServer.call(@name, :get_tsl_status)
  end

  ## Defining GenServer Callbacks

  @impl true
  def init(state) do
    Logger.info("Starting State Server")
    subscribe()
    {:ok, state}
  end

  defp subscribe do
    Phoenix.PubSub.subscribe(UpTally.PubSub, "atem")
    Phoenix.PubSub.subscribe(UpTally.PubSub, "tsl")
  end

  @impl true
  def handle_info({:updated, inputs}, state) do
    Logger.debug("Updating State")
    {:noreply, Map.put(state, :inputs, inputs)}
  end

  @impl true
  def handle_info(:connected, state) do
    Logger.debug("Updating State")
    broadcast_atem_update(:connected)
    {:noreply, Map.put(state, :atem_status, :connected)}
  end

  @impl true
  def handle_info(:disconnected, state) do
    Logger.debug("Updating State")
    broadcast_atem_update(:disconnected)
    {:noreply, Map.put(state, :atem_status, :disconnected)}
  end

  @impl true
  def handle_info(:tsl_connected, state) do
    Logger.debug("Updating State")
    broadcast_tsl_update(:connected)
    {:noreply, Map.put(state, :tsl_status, :connected)}
  end

  @impl true
  def handle_info(:tsl_disconnected, state) do
    Logger.debug("Updating State")
    broadcast_tsl_update(:disconnected)
    {:noreply, Map.put(state, :tsl_status, :disconnected)}
  end

  @impl true
  def handle_info({:names, data}, state) do
    inputs =
      data
      |> Map.values()
      |> Enum.reduce(%{}, fn %{
                               video_src: input_num,
                               long_name: input_name
                             } = _input,
                             inputs ->
        if input_num >= 1 and input_num <= 40 do
          case Map.get(inputs, input_num) do
            nil ->
              Map.put(inputs, input_num, %Input{
                input_number: input_num,
                name: input_name
              })

            input ->
              Map.put(inputs, input_num, Map.put(input, :name, input_name))
          end
        else
          inputs
        end
      end)

    state = Map.put(state, :inputs, inputs)

    broadcast_routing_update(inputs)

    {:noreply, state}
  end

  @impl true
  def handle_info({:routing, output_map}, state) do
    inputs =
      state.inputs
      |> reset_tally()
      |> set_tallies(output_map.me1_pgm, :on_pgm_me1)
      |> set_tallies(output_map.me1_pv, :on_pv_me1)
      |> set_tallies(output_map.me2_pgm, :on_pgm_me2)
      |> set_tallies(output_map.me2_pv, :on_pv_me2)
      |> set_tallies(output_map.me3_pgm, :on_pgm_me3)
      |> set_tallies(output_map.me3_pv, :on_pv_me3)
      |> set_tallies(output_map.me4_pgm, :on_pgm_me4)
      |> set_tallies(output_map.me4_pv, :on_pv_me4)

    state = Map.put(state, :inputs, inputs)
    broadcast_routing_update(state.inputs)
    {:noreply, state}
  end

  @impl true
  def handle_info(_, state) do
    {:noreply, state}
  end

  @impl true
  def handle_call(:get_inputs, _from, state) do
    {:reply, state.inputs, state}
  end

  @impl true
  def handle_call(:get_atem_status, _from, state) do
    {:reply, state.atem_status, state}
  end

  @impl true
  def handle_call(:get_tsl_status, _from, state) do
    {:reply, state.tsl_status, state}
  end

  defp broadcast_routing_update(inputs) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "state",
      {:routing, inputs}
    )
  end

  defp broadcast_atem_update(status) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "state",
      {:atem_status, status}
    )
  end

  defp broadcast_tsl_update(status) do
    Phoenix.PubSub.broadcast(
      UpTally.PubSub,
      "state",
      {:tsl_status, status}
    )
  end

  defp reset_tally(inputs) do
    Map.new(inputs, fn {key, value} ->
      {key,
       value
       |> Map.put(:on_pgm_me1, false)
       |> Map.put(:on_pv_me1, false)
       |> Map.put(:on_pgm_me2, false)
       |> Map.put(:on_pv_me2, false)
       |> Map.put(:on_pgm_me3, false)
       |> Map.put(:on_pv_me3, false)
       |> Map.put(:on_pgm_me4, false)
       |> Map.put(:on_pv_me4, false)}
    end)
  end

  defp set_tallies(inputs, output_map, key) do
    Map.new(inputs, fn {channel, value} ->
      {channel, value |> set_tally(output_map, key)}
    end)
  end

  defp set_tally(input, output_map, key) do
    if Enum.find(output_map, &(&1 == input.input_number)) do
      Map.put(input, key, true)
    else
      input
    end
  end
end
