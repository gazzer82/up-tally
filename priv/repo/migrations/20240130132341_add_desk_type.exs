defmodule UpTally.Repo.Migrations.AddDeskType do
  use Ecto.Migration

  def change do
    alter table(:atem) do
      add :model, :string, null: false, default: "constellation_2me"
    end
  end
end
