defmodule UpTally.Repo.Migrations.AddVideohubName do
  use Ecto.Migration

  def change do
    alter table(:videohub) do
      add :name, :string
    end
  end
end
