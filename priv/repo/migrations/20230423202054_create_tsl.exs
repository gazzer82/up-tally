defmodule UpTally.Repo.Migrations.CreateTsl do
  use Ecto.Migration

  def change do
    create table(:tsl) do
      add :name, :string
      add :ip, :string
      add :port, :integer
      add :interface, :string
      add :tsl_version, :string
      add :send_interval, :integer
      add :tsl_offset, :integer
      add :polling_interval, :integer
      add :polling_enabled, :boolean, default: true, null: true

      timestamps()
    end
  end
end
