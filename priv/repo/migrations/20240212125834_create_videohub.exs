defmodule UpTally.Repo.Migrations.CreateVideohub do
  use Ecto.Migration

  def change do
    create table(:videohub) do
      add :ip, :string
      add :port, :integer

      timestamps()
    end
  end
end
