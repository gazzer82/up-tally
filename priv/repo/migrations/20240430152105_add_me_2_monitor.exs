defmodule UpTally.Repo.Migrations.AddMe2Monitor do
  use Ecto.Migration

  def change do
    alter table(:tsl) do
      add :me1, :boolean, default: true, null: true
      add :me2, :boolean, default: true, null: true
      add :me3, :boolean, default: false, null: false
      add :me4, :boolean, default: false, null: false
    end
  end
end
