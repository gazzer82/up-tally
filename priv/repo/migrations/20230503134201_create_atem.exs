defmodule UpTally.Repo.Migrations.CreateAtem do
  use Ecto.Migration

  def change do
    create table(:atem) do
      add :ip, :string
      add :port, :integer
      add :test, :boolean
      timestamps()
    end
  end
end
