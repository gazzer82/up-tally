# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     UpTally.Repo.insert!(%UpTally.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

UpTally.Repo.insert!(%UpTally.Services.Tsl{
  interface: UpTally.Interfaces.get_default_interface_string(),
  ip: "239.1.2.3",
  name: "Eng Racks",
  port: 40001,
  tsl_version: :tsl_v3,
  send_interval: 50,
  polling_interval: 200,
  polling_enabled: true,
  tsl_offset: 0
})

UpTally.Repo.insert!(%UpTally.Services.Atem{
  ip: "192.168.10.240",
  port: 9910
})
